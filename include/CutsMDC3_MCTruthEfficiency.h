#ifndef CutsMDC3_MCTruthEfficiency_H
#define CutsMDC3_MCTruthEfficiency_H

#include "EventBase.h"

class CutsMDC3_MCTruthEfficiency  {

public:
  CutsMDC3_MCTruthEfficiency(EventBase* eventBase);
  ~CutsMDC3_MCTruthEfficiency();
  bool MDC3_MCTruthEfficiencyCutsOK();
    
  pair<vector<pair<int,float>>, vector<pair<int,float>>> GetAllSXs();

  static bool sortinrev(const pair<int,double> &a, const pair<int,double> &b){
      return (a.second < b.second);
    }

  //static bool sortinrevint(const pair<int,float> &a, const pair<int,float> &b){
      //return (a.second > b.second);
    //}

private:

  EventBase* m_event;

};

#endif
