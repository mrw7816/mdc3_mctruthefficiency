#include "CutsMDC3_MCTruthEfficiency.h"
#include "ConfigSvc.h"

CutsMDC3_MCTruthEfficiency::CutsMDC3_MCTruthEfficiency(EventBase* eventBase) {
  m_event = eventBase;
}

CutsMDC3_MCTruthEfficiency::~CutsMDC3_MCTruthEfficiency() {
  
}

// Function that lists all of the common cuts for this Analysis
bool CutsMDC3_MCTruthEfficiency::MDC3_MCTruthEfficiencyCutsOK() {
  // List of common cuts for this analysis into one cut
  return true;
}

pair<vector<pair<int,float>>, vector<pair<int,float>>> CutsMDC3_MCTruthEfficiency::GetAllSXs() {

  vector<pair<int, float>> s1pulses;
  vector<pair<int, float>> s2pulses;
  s1pulses.push_back(make_pair(-1, float(0)));
  s2pulses.push_back(make_pair(-1, float(0)));
  
  for (int p=0; p<(*m_event->m_tpcPulses)->nPulses; ++p) {
    bool s1prob = (*m_event->m_tpcPulses)->s1Probability[p];
    bool s2prob = (*m_event->m_tpcPulses)->s2Probability[p];
    
    if (s2prob == 1) {
      if (s2pulses.front() == make_pair(-1,float(0))) s2pulses.erase(s2pulses.begin());
      s2pulses.push_back(make_pair((*m_event->m_tpcPulses)->pulseID[p], (*m_event->m_tpcPulses)->pulseArea_phd[p]));
    }
    
    if (s1prob == 1) {
      if (s1pulses.front() == make_pair(-1,float(0))) s1pulses.erase(s1pulses.begin());
      s1pulses.push_back(make_pair((*m_event->m_tpcPulses)->pulseID[p], (*m_event->m_tpcPulses)->pulseArea_phd[p]));
    }
  }

  sort(s2pulses.begin(), s2pulses.end(), sortinrev);
  sort(s1pulses.begin(), s1pulses.end(), sortinrev);

  return make_pair(s1pulses,s2pulses);
}