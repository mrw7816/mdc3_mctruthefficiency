#include "EventBase.h"
#include "Analysis.h"
#include "HistSvc.h"
#include "Logger.h"
#include "ConfigSvc.h"
#include "SkimSvc.h"
#include "SparseSvc.h"
#include "CutsBase.h"
#include "CutsMDC3_MCTruthEfficiency.h"
#include "MDC3_MCTruthEfficiency.h"
#include "TF1.h"
#include "TH2F.h"
namespace {

  void Loggify(TAxis* axis) {
    int bins = axis->GetNbins();

    Axis_t from = log10(axis->GetXmin());
    Axis_t to = log10(axis->GetXmax());
    Axis_t width = (to - from) / bins;
    Axis_t *new_bins = new Axis_t[bins + 1];

    for (int i = 0; i <= bins; i++) new_bins[i] = pow(10, from + i * width);
    axis->Set(bins, new_bins);
    delete[] new_bins;
  }

  TH1* LoggifyX(TH1* h)  { Loggify(h->GetXaxis()); return h; }
  TH2* LoggifyXY(TH2* h) { Loggify(h->GetXaxis()); Loggify(h->GetYaxis()); return h;}
  TH2* LoggifyX(TH2* h)  { Loggify(h->GetXaxis()); return h; }
  TH2* LoggifyY(TH2* h)  { Loggify(h->GetYaxis()); return h; }

}

// Constructor
MDC3_MCTruthEfficiency::MDC3_MCTruthEfficiency()
  : Analysis()
{

  // Set up the expected event structure, include branches required for analysis.
  // List the branches required below, see full list here https://luxzeplin.gitlab.io/docs/softwaredocs/analysis/analysiswithrqs/rqlist.html
  // Load in the Single Scatters Branch
  m_event->IncludeBranch("pulsesTPC");
  m_event->IncludeBranch("pulsesODHG");
  m_event->IncludeBranch("pulsesSkin");
  m_event->IncludeBranch("ss");
  m_event->IncludeBranch("ms");
  m_event->IncludeBranch("pileUp");
  m_event->IncludeBranch("kr83m");
  m_event->IncludeBranch("other");
  m_event->IncludeBranch("mcTruthVertices");
  m_event->IncludeBranch("mcTruthPulses");
  //m_event->IncludeBranch("mcTruthDarkCounts");
  m_event->Initialize();
  ////////

  // Setup logging
  logging::set_program_name("MDC3_MCTruthEfficiency Analysis");

  // Setup the analysis specific cuts.
  m_cutsMDC3_MCTruthEfficiency = new CutsMDC3_MCTruthEfficiency(m_event);

  // Setup config service so it can be used to get config variables
  m_conf = ConfigSvc::Instance();


}

// Destructor
MDC3_MCTruthEfficiency::~MDC3_MCTruthEfficiency() {
  delete m_cutsMDC3_MCTruthEfficiency;
}

/////////////////////////////////////////////////////////////////
// Start of main analysis code block
//
// Initialize() -  Called once before the event loop.
void MDC3_MCTruthEfficiency::Initialize() {
  INFO("Initializing MDC3_MCTruthEfficiency Analysis");
  //initialize mdc3 core cuts
  m_cuts->mdc3()->Initialize();
    
  //parentname = 'null';  // initialize these two variables for mctruth etrain cutting since cutting an LZAP etrain may not cut a truth etrain!
  //parenttime = 0.0;  

  TH1* htmp = NULL; // ALPACA owns the booked histogram. We use temp pointers to manipulate   the histograms.

  int nBinsS1 = 500;
  float minS1 = 7e-1;
  float maxS1 = 3e5;

  int nBinsS2 = 500;
  float minS2 = 1e2;
  float maxS2 = 5e7;
    
  int nBinsS1ratio = 500;
  float minS1ratio = 1e-6;
  float maxS1ratio = 1.1;

  int   nBinsS2ratio = 500;
  float minS2ratio   = 1e-6;
  float maxS2ratio   = 1.1;

  int   nBinsH2W = 500;
  float minH2W   = 1e-6;
  float maxH2W   = 10;
    
  //SS
  htmp = m_hists->BookHist("ss_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("ss_s1_s2_100", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("ss_Tms_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("SS_TruthRawS1VsS1area", 100, 0, 10000, 50, 0, 5000);
  htmp = m_hists->BookHist("SS_TruthHitS1VsS1area", 100, 0, 10000, 50, 0, 5000);
  htmp = m_hists->BookHist("SS_TruthDetectedS1VsS1area", 100, 0, 10000, 50, 0, 5000);
  htmp = m_hists->BookHist("SS_TruthDetectedS1VsTruthRaw", 100, 0, 10000, 50, 0, 5000);
  htmp = m_hists->BookHist("SS_TruthRawS1VsDataS1", 100, 0, 10000, 50, 0, 5000);
  htmp = m_hists->BookHist("SS_TruthHitS1VsDataS1", 100, 0, 10000, 50, 0, 5000);
  htmp = m_hists->BookHist("SS_TruthDetectedS1VsDataS1", 100, 0, 10000, 50, 0, 5000);

  htmp = m_hists->BookHist("SS_DataS1DivRawS1", 100, 0.0, 1.0);
  htmp = m_hists->BookHist("SS_DataS1DivHitS1", 100, 0.0, 1.0);
  htmp = m_hists->BookHist("SS_DataS1DivDetectedS1", 100, 0.0, 1.0);
  htmp = m_hists->BookHist("SS_DetectedDivRaw", 100, 0.0, 1.0);
  htmp = m_hists->BookHist("SS_S1areaDivRawS1", 100, 0.0, 1.0);
  htmp = m_hists->BookHist("SS_S1areaDivHitS1", 100, 0.0, 1.0);
  htmp = m_hists->BookHist("SS_S1areaDivDetectedS1", 100, 0.0, 1.0);

  m_hists->BookHist("SS_eventType_Truth", 5, 0, 5);   
  m_hists->BookHist("SS_eventType_Truth_et", 5, 0, 5); 
  m_hists->BookHist("SS_eventType_Truth_fid", 5, 0, 5);
  m_hists->BookHist("SS_eventType_Truth_veto", 5, 0, 5);
  //MS
  htmp = m_hists->BookHist("ms_s1_s2sum", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("MS_TruthRawS1VsS1area", 100, 0, 10000, 50, 0, 5000);
  htmp = m_hists->BookHist("MS_TruthHitS1VsS1area", 100, 0, 10000, 50, 0, 5000);
  htmp = m_hists->BookHist("MS_TruthDetectedS1VsS1area", 100, 0, 10000, 50, 0, 5000);
  htmp = m_hists->BookHist("MS_TruthDetectedS1VsTruthRaw", 100, 0, 10000, 50, 0, 5000);

  htmp = m_hists->BookHist("MS_DetectedDivRaw", 100, 0.0, 1.0);
  htmp = m_hists->BookHist("MS_S1areaDivRawS1", 100, 0.0, 1.0);
  htmp = m_hists->BookHist("MS_S1areaDivHitS1", 100, 0.0, 1.0);
  htmp = m_hists->BookHist("MS_S1areaDivDetectedS1", 100, 0.0, 1.0);

  m_hists->BookHist("MS_eventType_Truth", 5, 0, 5);  
  m_hists->BookHist("MS_eventType_Truth_et", 5, 0, 5); 
  m_hists->BookHist("MS_eventType_Truth_fid", 5, 0, 5);
  m_hists->BookHist("MS_eventType_Truth_veto", 5, 0, 5);    
  //Kr
  htmp = m_hists->BookHist("kr_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("kr_Tss_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("KR_TruthRawS1VsS1area", 100, 0, 10000, 50, 0, 5000);
  htmp = m_hists->BookHist("KR_TruthHitS1VsS1area", 100, 0, 10000, 50, 0, 5000);
  htmp = m_hists->BookHist("KR_TruthDetectedS1VsS1area", 100, 0, 10000, 50, 0, 5000);
  htmp = m_hists->BookHist("KR_TruthDetectedS1VsTruthRaw", 100, 0, 10000, 50, 0, 5000);

  htmp = m_hists->BookHist("KR_DetectedDivRaw", 100, 0.0, 1.0);
  htmp = m_hists->BookHist("KR_S1areaDivRawS1", 100, 0.0, 1.0);
  htmp = m_hists->BookHist("KR_S1areaDivHitS1", 100, 0.0, 1.0);
  htmp = m_hists->BookHist("KR_S1areaDivDetectedS1", 100, 0.0, 1.0);

  m_hists->BookHist("KR_eventType_Truth", 5, 0, 5);    
  m_hists->BookHist("KR_eventType_Truth_et", 5, 0, 5); 
  m_hists->BookHist("KR_eventType_Truth_fid", 5, 0, 5);
  m_hists->BookHist("KR_eventType_Truth_veto", 5, 0, 5);    
  //PU
  htmp = m_hists->BookHist("pu_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("pu_Tss_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("PU_TruthRawS1VsS1area", 100, 0, 10000, 50, 0, 5000);
  htmp = m_hists->BookHist("PU_TruthHitS1VsS1area", 100, 0, 10000, 50, 0, 5000);
  htmp = m_hists->BookHist("PU_TruthDetectedS1VsS1area", 100, 0, 10000, 50, 0, 5000);
  htmp = m_hists->BookHist("PU_TruthDetectedS1VsTruthRaw", 100, 0, 10000, 50, 0, 5000);

  htmp = m_hists->BookHist("PU_DetectedDivRaw", 100, 0.0, 1.0);
  htmp = m_hists->BookHist("PU_S1areaDivRawS1", 100, 0.0, 1.0);
  htmp = m_hists->BookHist("PU_S1areaDivHitS1", 100, 0.0, 1.0);
  htmp = m_hists->BookHist("PU_S1areaDivDetectedS1", 100, 0.0, 1.0);

  m_hists->BookHist("PU_eventType_Truth", 5, 0, 5);  
  m_hists->BookHist("PU_eventType_Truth_et", 5, 0, 5); 
  m_hists->BookHist("PU_eventType_Truth_fid", 5, 0, 5);
  m_hists->BookHist("PU_eventType_Truth_veto", 5, 0, 5);    
  //Other
  htmp = m_hists->BookHist("other_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("OTHER_TruthRawS1VsS1area", 100, 0, 10000, 50, 0, 5000);
  htmp = m_hists->BookHist("OTHER_TruthHitS1VsS1area", 100, 0, 10000, 50, 0, 5000);
  htmp = m_hists->BookHist("OTHER_TruthDetectedS1VsS1area", 100, 0, 10000, 50, 0, 5000);
  htmp = m_hists->BookHist("OTHER_TruthDetectedS1VsTruthRaw", 100, 0, 10000, 50, 0, 5000);
  

  htmp = m_hists->BookHist("OTHER_DetectedDivRaw", 100, 0.0, 1.0);
  htmp = m_hists->BookHist("OTHER_S1areaDivRawS1", 100, 0.0, 1.0);
  htmp = m_hists->BookHist("OTHER_S1areaDivHitS1", 100, 0.0, 1.0);
  htmp = m_hists->BookHist("OTHER_S1areaDivDetectedS1", 100, 0.0, 1.0);

  m_hists->BookHist("OTHER_eventType_Truth", 5, 0, 5);    
  m_hists->BookHist("OTHER_eventType_Truth_et", 5, 0, 5); 
  m_hists->BookHist("OTHER_eventType_Truth_fid", 5, 0, 5);
  m_hists->BookHist("OTHER_eventType_Truth_veto", 5, 0, 5);    
  // Event type
  m_hists->BookHist("pulseType", 12, 0, 12);
  m_hists->BookHist("pulseTime", 1000, 0, 3000000,12, 0, 12);
  m_hists->BookHist("eventType_Truth", 5, 0, 5);
  m_hists->BookHist("eventType_Truth_et", 5, 0, 5);
  m_hists->BookHist("eventType_et", 5, 0, 5);
  m_hists->BookHist("nS1", 10, 0, 10);
  m_hists->BookHist("nS2", 10, 0, 10);
  m_hists->BookHist("nS1b4time", 10, 0, 10);
  m_hists->BookHist("nS2b4time", 10, 0, 10);
  m_hists->BookHist("otherType", 5, 0, 5);

  m_hists->BookHist("SS_LZAPdivTruth", 1, 0, 1);
  m_hists->BookHist("MS_LZAPdivTruth", 1, 0, 1);
  m_hists->BookHist("KR_LZAPdivTruth", 1, 0, 1);
  m_hists->BookHist("PU_LZAPdivTruth", 1, 0, 1);
  m_hists->BookHist("O_LZAPdivTruth", 1, 0, 1);

  htmp = m_hists->BookHist("all_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("all_et_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("all_net_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  
  htmp = m_hists->BookHist("all_et_s1ratio_h2w_allS1s", nBinsS1ratio, minS1ratio, maxS1ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("all_et_s1ratio_h2w_firstS1", nBinsS1ratio, minS1ratio, maxS1ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("all_et_s2ratio_h2w_allS2s", nBinsS2ratio, minS2ratio, maxS2ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("all_et_s2ratio_h2w_firstS2", nBinsS2ratio, minS2ratio, maxS2ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
    
  htmp = m_hists->BookHist("all_s1_s2_missed", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("all_et_s1_s2_missed", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("all_net_s1_s2_missed", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  
  htmp = m_hists->BookHist("all_et_s1ratio_h2w_allS1s_missed", nBinsS1ratio, minS1ratio, maxS1ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("all_et_s1ratio_h2w_firstS1_missed", nBinsS1ratio, minS1ratio, maxS1ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("all_et_s2ratio_h2w_allS2s_missed", nBinsS2ratio, minS2ratio, maxS2ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("all_et_s2ratio_h2w_firstS2_missed", nBinsS2ratio, minS2ratio, maxS2ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
    
  // Empty histograms just for plotting the S1 and S2 cut functions
  htmp = m_hists->BookHist("h_s1cut_lzap", nBinsS1ratio, minS1ratio, maxS1ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("h_s2cut_lzap", nBinsS2ratio, minS2ratio, maxS2ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);

}


// Execute() - Called once per event.
void MDC3_MCTruthEfficiency::Execute() {
  m_cuts->mdc3()->InitializeETrainVeto();
    
  //Get some truth info
  int numV = (*m_event->m_TruthVertices)->nRQMCTruthVertices;
  int totalTruthRawS1s = 0;
  int totalTruthHitS1s = 0;
  int totalTruthDetectedS1s = 0;
  for (int i=0; i < numV; i++){
      totalTruthRawS1s+=(*m_event->m_TruthVertices)->rawS1Photons[i];
      totalTruthHitS1s+=(*m_event->m_TruthVertices)->s1PhotonHits[i];
      totalTruthDetectedS1s+=(*m_event->m_TruthVertices)->detectedS1Photons[i];
    } 
  //Figure out how many S1s and S2s were actually in an event
  int nPulses = (*m_event->m_TruthPulses)->nRQMCTruthPulses;
  vector<unsigned short> P_type = (*m_event->m_TruthPulses)->pulseIdentifier;
  vector<double> phe_start = (*m_event->m_TruthPulses)->firstPheTime_ns;
  vector<double> phe_end = (*m_event->m_TruthPulses)->lastPheTime_ns;
  vector<unsigned int> detpho1 = (*m_event->m_TruthVertices)->detectedS1Photons;
  vector<unsigned int> detpho2 = (*m_event->m_TruthVertices)->detectedS2Photons;
  vector<pair<int,double>> s1pulse;
  vector<pair<int,double>> s2pulse;
  vector<double> s1start;
  vector<double> s2start;
  vector<double> s1end;
  vector<double> s2end;
  vector<double> s1mean;
  vector<double> s2mean;
  int nS1 = 0;
  int nS2 = 0;    
  vector<int> S1s = (*m_event->m_TruthVertices)->s1PulseIndex;  
  vector<int> S2s = (*m_event->m_TruthVertices)->s2PulseIndex;   
  vector<int> TruthS1s; 
  vector<int> TruthS2s;  
  vector<int> properS1s;
  vector<int> properS2s;
  vector<int> S1;
  vector<int> S2;
  double ParentX = (*m_event->m_TruthEvent)->parentPositionX_mm;
  double ParentY = (*m_event->m_TruthEvent)->parentPositionY_mm;
  double ParentZ = (*m_event->m_TruthEvent)->parentPositionZ_mm;    
    
  
    // have to recast the vectors since we can't make changes to the original lzap ones
  for(int i=0;i<S1s.size(); i++){
    TruthS1s.push_back(S1s[i]);
  }
  for(int i=0;i<S2s.size(); i++){
    TruthS2s.push_back(S2s[i]);
  } 
   // take only vertices that have a measured photon count 
  for(int i=0;i<TruthS1s.size();i++){
      if (detpho1[i] > 0){
          properS1s.push_back(TruthS1s[i]);
      }
  }
  for(int i=0;i<TruthS2s.size();i++){
      if (detpho2[i] > 0){
          properS2s.push_back(TruthS2s[i]);
      }
  }
  // get rid of vertices that didn't interact in the tpc
  properS1s.erase(std::remove(properS1s.begin(), properS1s.end(), -1), properS1s.end());
  properS2s.erase(std::remove(properS2s.begin(), properS2s.end(), -1), properS2s.end()); 

  nS1 += properS1s.size();
  nS2 += properS2s.size();
  //std::cout << "ns1" << nS1 << std::endl;     
    //make some vector pairs of the pulse IDs and the pulse start times
    if (nS1 != 0){      
      for (int n=0; n<nS1;n++){
         s1pulse.push_back(make_pair(properS1s[n],phe_start[properS1s[n]] / 1000));
         //std::cout << "filled s1start " << s1start.size() << std::endl;
      }
    }
    if (nS2 != 0){
      for (int n=0; n<nS2;n++){
         s2pulse.push_back(make_pair(properS2s[n],phe_start[properS2s[n]] / 1000));
      }
    }
  // these pulse IDs are not in time order (which is stupid) so they are sorted here (s1s and s2s are sorted seperately)
  sort(s1pulse.begin(), s1pulse.end(), sortinrev);
  sort(s2pulse.begin(), s2pulse.end(), sortinrev);

  //make some basic plots, but also seperate the S1 and S2s based on their timings
  for (int n=0; n<nPulses; n++){
      m_hists->GetHistFromMap("pulseType")->Fill(P_type[n]);
      m_hists->GetHistFromMap("pulseTime")->Fill(phe_start[n],P_type[n]);
  }   
  m_hists->GetHistFromMap("nS1b4time")->Fill(nS1); //how many total s1s and s2s were there
  m_hists->GetHistFromMap("nS2b4time")->Fill(nS2); 
    
    // now that they are all sorted, just grab the start times
    vector<float> s1times;
    for(int n=0; n<s1pulse.size(); n++){
        s1times.push_back(s1pulse[n].second);
    }
    vector<float> s2times;
    for(int n=0; n<s2pulse.size(); n++){
        s2times.push_back(s2pulse[n].second);
    }
    vector<float> alltimes;
    vector<int> s1ids;
    vector<int> s2ids;
    // now comes the messy process of putting the S1s and S2s in their correct time order with respect to each other
    int index{0};
    for (int i = 0; i < s1times.size(); ++i) {  //push both the s1s and s2s into the same vector
        alltimes.push_back(s1times[i]);
    }
    for (int i = 0; i < s2times.size(); ++i) {
        alltimes.push_back(s2times[i]);
    }
    // Loop over all times now
    std::vector<float>::iterator s1it;
    for (auto &&time: alltimes) {
        s1it = std::find(s1times.begin(), s1times.end(), time);
        // If not in s1 must be s2
        if (s1it == s1times.end()) {
            s2ids.push_back(index);
        }
        else {
            s1ids.push_back(index);
        }
        ++index;
    }   
  // now that they are all sorted with respect to each other, we need the relative seperation times between successive pulses and to tag pulses I think won't be resolved
  if (s1pulse.size() > 1) {
    //std::cout << "start s1 " << std::endl;
    for (int i=1; i<=s1pulse.size(); i++){
        //std::cout << "diff1 " << abs(s1pulse[i].second -s1pulse[i-1].second) << std::endl;
        if (abs(s1pulse[i].second -s1pulse[i-1].second) < 1.8 && nS1 > 1){   // if the pulses start too close to resolve, the detector would actually only see one real pulse
          nS1 -= 1;  
          s1ids[i] = 1000000;
        }
    }
  }   
  
  if (s2pulse.size() > 1) {
    for (int i=0; i<s2pulse.size(); i++){
        //std::cout << "diff2 " << abs(s2pulse[i].second - s2pulse[i+1].second) << std::endl;
        if (abs(s2pulse[i].second -s2pulse[i+1].second) < 1.8 && nS2 > 1){   // if the pulses start too close to resolve, the detector would actually only see one real pulse
          //std::cout << "ns2b4 " << nS2 << std::endl;
          nS2 -= 1; 
          s2ids[i] = -2;
        }
    }
  }  
  // get rid of the pulses that won't be resolved
  for(int i=0; i<s1ids.size(); i++){
      if (s1ids[i] != 1000000){
          S1.push_back(s1ids[i]);
      }
  } 
    
  for(int i=0; i<s2ids.size(); i++){
      if (s2ids[i] != -2){
          S2.push_back(s2ids[i]);
      }
  }
    // now regrab the inital sorted pulse IDs, not the ones we assigned earlier 
  for(int n=0; n<S1.size(); n++){
      s1start.push_back(s1pulse[S1[n]].second);
  }
  for(int n=0; n<S2.size(); n++){
      s2start.push_back(s2pulse[S2[n]].second);
  }

  m_hists->GetHistFromMap("nS1")->Fill(nS1); //how many total s1s and s2s were there
  m_hists->GetHistFromMap("nS2")->Fill(nS2);    

  // Find how many S1s are before the last S2
  int lastS2 = -1;
  for (int p=0; p<S2.size(); p++) {
    if (S2[p] > lastS2) lastS2 = S2[p];
  }

  vector<int> prominentS1sBeforeLastS2;
  for (int n=0; n<S1.size(); n++) { // for all the prominent S1s
    if (S1[n] < lastS2) { // add only the ones that are before the last S2
      prominentS1sBeforeLastS2.push_back(S1[n]);
    }
  }
  //std::cout << "prominentS1sBeforeLastS2 "  << std::endl;
  // Find how many S2s are after the first S1
  int firstS1 = 999999;
  for (int p=0; p<prominentS1sBeforeLastS2.size(); p++) {
    if (prominentS1sBeforeLastS2[p] < firstS1) firstS1 = prominentS1sBeforeLastS2[p];
  }

  vector<int> prominentS2sAfterFirstS1;
  for (int n=0; n<S2.size(); n++) { // for all the prominent S2s
    if (S2[n] > firstS1) { // add only the ones that are after the first S1
      prominentS2sAfterFirstS1.push_back(S2[n]);
    }
  }
  //std::cout << "prominentS2sAfterFirstS1 "  << std::endl;
  int multiS1 = prominentS1sBeforeLastS2.size();
  int multiS2 = prominentS2sAfterFirstS1.size();
  //std::cout << "multiS1 " << multiS1 << std::endl;
  //std::cout << "multiS2 " << multiS2 << std::endl;

  bool tSS = false; // type of event truth thinks it is
  bool tMS = false;
  bool tKr = false;
  bool tPU = false;
  bool tO = false;  
    
  // event topology creation which will be very messy 

  
  //*****Do the new Interaction Finding - dev version*************
  if (multiS2 == 1) { // If there is just 1 prominent S2
    if (multiS1 == 1) { // if there is just 1 prominent S1 before the prominent S2
      tSS = true;
      truthSS += 1;
    } // end single S1

    if (multiS1 == 2) { // if there are 2 prominent S1s before the single prominent S2, check if Kr          
      // if they are nearby (25 half lives), and the bigger one comes first, then it's a Kr event
      if (abs(s1start[0] - s1start[1]) <= 154*25) {
	tKr = true;
  truthKr += 1;
      }
    } // end 2 S1

    // If there is 1 prominent S2 and more than 2 prominent S1s, it is automatically an Other event
  } //end single S2
        
  if (multiS2 > 1) { // if there is more than 1 prominent S2 after the first S1
    if (multiS1 == 1) { // if there is a single prominent S1, that comes before multiple prominent S2s
      tMS = true; // then MS
      truthMS += 1;
    } // end single S1
    
    if (multiS1 > 1) { // if there is more than 1 prominent S1 (that comes before the last S2)
      tPU = true; // then PU 
      truthPU += 1;
    } // end multi S1
  } // end multi S2

  if (!tSS && !tMS && !tKr && !tPU){
     tO = true;
     truthO += 1;
  }
    
      
    
  //end truth analysis and start lzap analysis
    
  int numSS = (*m_event->m_singleScatter)->nSingleScatters;
  int numMS = (*m_event->m_multipleScatter)->nMultipleScatters;
  int numKr = (*m_event->m_kr83mScatter)->nKr83mScatters;
  int numPU = (*m_event->m_pileupScatter)->nPileUpScatters;
  int numOther = (*m_event->m_otherScatter)->nOtherScatters; 
    
  bool SS_tMS_v = false;
  bool SS_tKR_et = false;
  bool SS_tPU_et = false;
  bool MS_tSS_v = false;
  bool O_tSS_et = false;
  bool PU_tSS_et = false;
  bool KR_tSS_et = false;
    
  //SS
  if (numSS > 0){
    //std::cout << "isSS "  << std::endl;
    nSS += 1;
    float s1area = (*m_event->m_singleScatter)->s1Area_phd;
    float s2area = (*m_event->m_singleScatter)->s2Area_phd;
    float s1photoncount = (*m_event->m_singleScatter)->s1PhotonCount;
       
    if (!m_cuts->mdc3()->ETrainVeto() && s1area < 1500){  //implement etrain veto and count
        m_hists->GetHistFromMap("eventType_et")->Fill(0);
        nSS_et += 1;
        if (tSS){
           m_hists->GetHistFromMap("SS_eventType_Truth_et")->Fill(0);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(0);
           SS_truthSS_et += 1;
        }
        if (tMS){
           m_hists->GetHistFromMap("SS_eventType_Truth_et")->Fill(1);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(1);
           SS_truthMS_et += 1;                
        }
        if (tKr){
           m_hists->GetHistFromMap("SS_eventType_Truth_et")->Fill(2);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(2);
           SS_truthKR_et += 1;
           SS_tKR_et = true;
        }
        if (tPU){
           m_hists->GetHistFromMap("SS_eventType_Truth_et")->Fill(3);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(3);
           SS_truthPU_et += 1;     
           SS_tPU_et = true;
        }
        if (tO){
           m_hists->GetHistFromMap("SS_eventType_Truth_et")->Fill(4);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(4);
           SS_truthO_et += 1;                
        }
        if (m_cuts->mdc3()->Fiducial()){  //implement fid cut and count
            nSS_fid += 1;  
            if (tSS){
              m_hists->GetHistFromMap("SS_eventType_Truth_fid")->Fill(0);
              SS_truthSS_fid += 1;
            }
            if (tMS){
              m_hists->GetHistFromMap("SS_eventType_Truth_fid")->Fill(1);
              SS_truthMS_fid += 1;            
            }
            if (tKr){
              m_hists->GetHistFromMap("SS_eventType_Truth_fid")->Fill(2);
              SS_truthKR_fid += 1;
            }
            if (tPU){
              m_hists->GetHistFromMap("SS_eventType_Truth_fid")->Fill(3);
              SS_truthPU_fid += 1;              
            }
            if (tO){
              m_hists->GetHistFromMap("SS_eventType_Truth_fid")->Fill(4);
              SS_truthO_fid += 1;                
            }
            if (!m_cuts->mdc3()->SkinVeto() && !m_cuts->mdc3()->ODVeto()){  //implement veto cut and count
                nSS_veto += 1;
                m_hists->GetHistFromMap("ss_s1_s2")->Fill(s1area, s2area);
                if (tSS){
                    m_hists->GetHistFromMap("SS_eventType_Truth_veto")->Fill(0);
                    SS_truthSS_veto += 1;
                }
                if (tMS){
                    m_hists->GetHistFromMap("SS_eventType_Truth_veto")->Fill(1);
                    SS_truthMS_veto += 1; 
                    SS_tMS_v = true; 
                    m_hists->GetHistFromMap("ss_Tms_s1_s2")->Fill(s1area, s2area);                    
                }
                if (tKr){
                    m_hists->GetHistFromMap("SS_eventType_Truth_veto")->Fill(2);
                    SS_truthKR_veto += 1;
                    //ofstream outfile;                    
                    //outfile.open("/global/project/projectdirs/lz/users/mwilliams/LZAPSS_TruthKR.txt",std::ios_base::app);
                    //outfile<<"/global/cfs/cdirs/lz/data/MDC3/background/BACCARAT-4.11.0_DER-8.5.13/20180426/"
                      //     <<(*m_event->m_eventHeader)->rawFileName << " "
                        //   <<(*m_event->m_eventHeader)->runID << " "
                          // <<(*m_event->m_eventHeader)->eventID << "\n";
                    //outfile.close();
                }
                if (tPU){
                    m_hists->GetHistFromMap("SS_eventType_Truth_veto")->Fill(3);
                    SS_truthPU_veto += 1;   
                }
                if (tO){
                    m_hists->GetHistFromMap("SS_eventType_Truth_veto")->Fill(4);
                    SS_truthO_veto += 1;               
                }
              if(s1area < 100){
                SS_100phd += 1;
                m_hists->GetHistFromMap("ss_s1_s2_100")->Fill(s1area, s2area);
                if (tSS){
                    SS_truthSS_100phd += 1;
                } 
                if (tMS){
                SS_truthMS_100phd += 1;
                }
                if (tKr) SS_truthKR_100phd += 1;
                if (tPU) SS_truthPU_100phd += 1;
                if (tO)  SS_truthO_100phd += 1;
              }
              if(s1area > 100 && s1area < 500){
                SS_100to500phd += 1;
                if (tSS) SS_truthSS_100to500phd += 1;
                if (tMS) SS_truthMS_100to500phd += 1;
                if (tKr) SS_truthKR_100to500phd += 1;
                if (tPU) SS_truthPU_100to500phd += 1;
                if (tO)  SS_truthO_100to500phd += 1;
              }
              if(s1area > 500 && s1area < 1000){
                SS_500to1000phd += 1;
                if (tSS) SS_truthSS_500to1000phd += 1;
                if (tMS) SS_truthMS_500to1000phd += 1;
                if (tKr) SS_truthKR_500to1000phd += 1;
                if (tPU) SS_truthPU_500to1000phd += 1;
                if (tO)  SS_truthO_500to1000phd += 1;
              }
              if(s1area > 1000 && s1area < 1500){
                SS_1000to1500phd += 1;
                if (tSS) SS_truthSS_1000to1500phd += 1;
                if (tMS) SS_truthMS_1000to1500phd += 1;
                if (tKr) SS_truthKR_1000to1500phd += 1;
                if (tPU) SS_truthPU_1000to1500phd += 1;
                if (tO)  SS_truthO_1000to1500phd += 1;
              }
              if(s1area > 1500){
                SS_1500phd += 1;  
                if (tSS) SS_truthSS_1500phd += 1;
                if (tMS) SS_truthMS_1500phd += 1;
                if (tKr) SS_truthKR_1500phd += 1;
                if (tPU) SS_truthPU_1500phd += 1;
                if (tO)  SS_truthO_1500phd += 1;
            }             
          } 
        }
      }


    m_hists->GetHistFromMap("SS_TruthRawS1VsS1area")->Fill(totalTruthRawS1s, s1area);
    m_hists->GetHistFromMap("SS_TruthHitS1VsS1area")->Fill(totalTruthHitS1s, s1area);
    m_hists->GetHistFromMap("SS_TruthDetectedS1VsS1area")->Fill(totalTruthDetectedS1s, s1area);
    m_hists->GetHistFromMap("SS_TruthDetectedS1VsTruthRaw")->Fill(totalTruthDetectedS1s, totalTruthRawS1s);
    m_hists->GetHistFromMap("SS_TruthRawS1VsDataS1")->Fill(totalTruthRawS1s, s1photoncount);
    m_hists->GetHistFromMap("SS_TruthHitS1VsDataS1")->Fill(totalTruthHitS1s, s1photoncount);
    m_hists->GetHistFromMap("SS_TruthDetectedS1VsDataS1")->Fill(totalTruthDetectedS1s, s1photoncount);

    float fractionOfRawToData = float(s1photoncount)/float(totalTruthRawS1s);
    float fractionOfHitToData = float(s1photoncount)/float(totalTruthHitS1s);
    float fractionOfDetectedToData = float(s1photoncount)/float(totalTruthDetectedS1s);
    float DetectedDivRaw = float(totalTruthDetectedS1s)/float(totalTruthRawS1s);

    float fractionOfRawToS1 = float(s1area)/float(totalTruthRawS1s);
    float fractionOfHitToS1 = float(s1area)/float(totalTruthHitS1s);
    float fractionOfDetectedToS1 = float(s1area)/float(totalTruthDetectedS1s);

    m_hists->GetHistFromMap("SS_DataS1DivRawS1")->Fill(fractionOfRawToData);
    m_hists->GetHistFromMap("SS_DataS1DivHitS1")->Fill(fractionOfHitToData);
    m_hists->GetHistFromMap("SS_DataS1DivDetectedS1")->Fill(fractionOfDetectedToData);
    m_hists->GetHistFromMap("SS_DetectedDivRaw")->Fill(DetectedDivRaw);
    m_hists->GetHistFromMap("SS_S1areaDivRawS1")->Fill(fractionOfRawToS1);
    m_hists->GetHistFromMap("SS_S1areaDivHitS1")->Fill(fractionOfHitToS1);
    m_hists->GetHistFromMap("SS_S1areaDivDetectedS1")->Fill(fractionOfDetectedToS1);
   
  } 
    
    // end SS

  // start MS 
  
  if (numMS > 0){
    //std::cout << "isMS "  << std::endl;
    nMS += 1;
    float s1area = (*m_event->m_multipleScatter)->s1Area_phd;
    vector<float> s2area = (*m_event->m_multipleScatter)->s2Area_phd;
    int s2maxIndex = std::max_element(s2area.begin(),s2area.end()) - s2area.begin();
    int nS2s = (*m_event->m_multipleScatter)->nS2s;
    float drift = (*m_event->m_multipleScatter)->driftTime_ns[s2maxIndex];
    float radius = (*m_event->m_multipleScatter)->correctedX_cm[s2maxIndex]*1.e2 + (*m_event->m_multipleScatter)->correctedY_cm[(s2maxIndex)]*1.e2;
    float s1Start = (*m_event->m_tpcPulses)->pulseStartTime_ns[(*m_event->m_multipleScatter)->s1PulseID] + (*m_event->m_tpcPulses)->areaFractionTime1_ns[(*m_event->m_multipleScatter)->s1PulseID];
    float s2sum = 0;
    for (int n=0; n<nS2s; n++) {
        s2sum += s2area[n];
    }
      
    if (!m_cuts->mdc3()->ETrainVeto(s2maxIndex) && s1area < 1500){
        m_hists->GetHistFromMap("eventType_et")->Fill(1);
        nMS_et += 1;
        if (tSS){
           m_hists->GetHistFromMap("MS_eventType_Truth_et")->Fill(0);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(0);
           MS_truthSS_et += 1;
        }
        if (tMS){
           m_hists->GetHistFromMap("MS_eventType_Truth_et")->Fill(1);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(1);
           MS_truthMS_et += 1;                
        }
        if (tKr){
           m_hists->GetHistFromMap("MS_eventType_Truth_et")->Fill(2);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(2);
           MS_truthKR_et += 1;
        }
        if (tPU){
           m_hists->GetHistFromMap("MS_eventType_Truth_et")->Fill(3);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(3);
           MS_truthPU_et += 1;              
        }
        if (tO){
           m_hists->GetHistFromMap("MS_eventType_Truth_et")->Fill(4);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(4);
           MS_truthO_et += 1;                
        }        
        
        if (m_cuts->mdc3()->Fiducial(radius,drift)){
            nMS_fid += 1;
            if (tSS){
              m_hists->GetHistFromMap("MS_eventType_Truth_fid")->Fill(0);
              MS_truthSS_fid += 1;
            }
            if (tMS){
              m_hists->GetHistFromMap("MS_eventType_Truth_fid")->Fill(1);
              MS_truthMS_fid += 1;            
            }
            if (tKr){
              m_hists->GetHistFromMap("MS_eventType_Truth_fid")->Fill(2);
              MS_truthKR_fid += 1;
            }
            if (tPU){
              m_hists->GetHistFromMap("MS_eventType_Truth_fid")->Fill(3);
              MS_truthPU_fid += 1;              
            }
            if (tO){
              m_hists->GetHistFromMap("MS_eventType_Truth_fid")->Fill(4);
              MS_truthO_fid += 1;                
            }
            
            if (!m_cuts->mdc3()->SkinVeto(s1Start) && !m_cuts->mdc3()->ODVeto(s1Start)){
                nMS_veto += 1;
                m_hists->GetHistFromMap("ms_s1_s2sum")->Fill(s1area, s2sum);
                if (tSS){
                    m_hists->GetHistFromMap("MS_eventType_Truth_veto")->Fill(0);
                    MS_truthSS_veto += 1;
                    MS_tSS_v = true;
                }
                if (tMS){
                    m_hists->GetHistFromMap("MS_eventType_Truth_veto")->Fill(1);
                    MS_truthMS_veto += 1;            
                }
                if (tKr){
                    m_hists->GetHistFromMap("MS_eventType_Truth_veto")->Fill(2);
                    MS_truthKR_veto += 1;
                }
                if (tPU){
                    m_hists->GetHistFromMap("MS_eventType_Truth_veto")->Fill(3);
                    MS_truthPU_veto += 1;              
                }
                if (tO){
                    m_hists->GetHistFromMap("MS_eventType_Truth_veto")->Fill(4);
                    MS_truthO_veto += 1;               
                }                
              if(s1area < 100){
                MS_100phd += 1;
                if (tSS) MS_truthSS_100phd += 1;
                if (tMS) MS_truthMS_100phd += 1;
                if (tKr) MS_truthKR_100phd += 1;
                if (tPU) MS_truthPU_100phd += 1;
                if (tO)  MS_truthO_100phd += 1;
              }
              if(s1area > 100 && s1area < 500){
                MS_100to500phd += 1;
                if (tSS) MS_truthSS_100to500phd += 1;
                if (tMS) MS_truthMS_100to500phd += 1;
                if (tKr) MS_truthKR_100to500phd += 1;
                if (tPU) MS_truthPU_100to500phd += 1;
                if (tO)  MS_truthO_100to500phd += 1;
              }
              if(s1area > 500 && s1area < 1000){
                MS_500to1000phd += 1;
                if (tSS) MS_truthSS_500to1000phd += 1;
                if (tMS) MS_truthMS_500to1000phd += 1;
                if (tKr) MS_truthKR_500to1000phd += 1;
                if (tPU) MS_truthPU_500to1000phd += 1;
                if (tO)  MS_truthO_500to1000phd += 1;
              }
              if(s1area > 1000 && s1area < 1500){
                MS_1000to1500phd += 1;
                if (tSS) MS_truthSS_1000to1500phd += 1;
                if (tMS) MS_truthMS_1000to1500phd += 1;
                if (tKr) MS_truthKR_1000to1500phd += 1;
                if (tPU) MS_truthPU_1000to1500phd += 1;
                if (tO)  MS_truthO_1000to1500phd += 1;
              }
              if(s1area > 1500){
                MS_1500phd += 1;  
                if (tSS) MS_truthSS_1500phd += 1;
                if (tMS) MS_truthMS_1500phd += 1;
                if (tKr) MS_truthKR_1500phd += 1;
                if (tPU) MS_truthPU_1500phd += 1;
                if (tO)  MS_truthO_1500phd += 1;
               }
            }            
        }
    }
      

    m_hists->GetHistFromMap("MS_TruthRawS1VsS1area")->Fill(totalTruthRawS1s, s1area);
    m_hists->GetHistFromMap("MS_TruthHitS1VsS1area")->Fill(totalTruthHitS1s, s1area);
    m_hists->GetHistFromMap("MS_TruthDetectedS1VsS1area")->Fill(totalTruthDetectedS1s, s1area);
    m_hists->GetHistFromMap("MS_TruthDetectedS1VsTruthRaw")->Fill(totalTruthDetectedS1s, totalTruthRawS1s);

    float fractionOfRawToS1 = float(s1area)/float(totalTruthRawS1s);
    float fractionOfHitToS1 = float(s1area)/float(totalTruthHitS1s);
    float fractionOfDetectedToS1 = float(s1area)/float(totalTruthDetectedS1s);
    float DetectedDivRaw = float(totalTruthDetectedS1s)/float(totalTruthRawS1s);

    m_hists->GetHistFromMap("MS_DetectedDivRaw")->Fill(DetectedDivRaw);
    m_hists->GetHistFromMap("MS_S1areaDivRawS1")->Fill(fractionOfRawToS1);
    m_hists->GetHistFromMap("MS_S1areaDivHitS1")->Fill(fractionOfHitToS1);
    m_hists->GetHistFromMap("MS_S1areaDivDetectedS1")->Fill(fractionOfDetectedToS1);      
  } 
  
  //end MS

  //start Kr
  if (numKr > 0){
    nKr += 1;
    float s1a = (*m_event->m_kr83mScatter)->s1aArea_phd;
    float s1b = (*m_event->m_kr83mScatter)->s1bArea_phd;
    float s1c = s1a + s1b;
    float s2area = (*m_event->m_kr83mScatter)->s2Area_phd;
    float s2ID = (*m_event->m_kr83mScatter)->s2PulseID;
    float drift = (*m_event->m_kr83mScatter)->driftTime_ns;
    float radius = (*m_event->m_kr83mScatter)->correctedX_cm*1.e2 + (*m_event->m_kr83mScatter)->correctedY_cm*1.e2;
    float s1Start = (*m_event->m_tpcPulses)->pulseStartTime_ns[(*m_event->m_kr83mScatter)->s1aPulseID] + (*m_event->m_tpcPulses)->areaFractionTime1_ns[(*m_event->m_kr83mScatter)->s1aPulseID];      
      
    if (!m_cuts->mdc3()->ETrainVeto(s2ID) && s1c < 1500){
        m_hists->GetHistFromMap("eventType_et")->Fill(2);
        nKr_et += 1;
        if (tSS){
           m_hists->GetHistFromMap("KR_eventType_Truth_et")->Fill(0);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(0);
           KR_truthSS_et += 1;
           KR_tSS_et = true;
        }
        if (tMS){
           m_hists->GetHistFromMap("KR_eventType_Truth_et")->Fill(1);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(1);
           KR_truthMS_et += 1;                
        }
        if (tKr){
           m_hists->GetHistFromMap("KR_eventType_Truth_et")->Fill(2);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(2);
           KR_truthKR_et += 1;
        }
        if (tPU){
           m_hists->GetHistFromMap("KR_eventType_Truth_et")->Fill(3);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(3);
           KR_truthPU_et += 1;              
        }
        if (tO){
           m_hists->GetHistFromMap("KR_eventType_Truth_et")->Fill(4);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(4);
           KR_truthO_et += 1;                
        }         
        
        if (m_cuts->mdc3()->Fiducial(radius,drift)){
            nKr_fid += 1;
            if (tSS){ 
              m_hists->GetHistFromMap("KR_eventType_Truth_fid")->Fill(0);
              KR_truthSS_fid += 1;
            }
            if (tMS){
              m_hists->GetHistFromMap("KR_eventType_Truth_fid")->Fill(1);
              KR_truthMS_fid += 1;            
            }
            if (tKr){
              m_hists->GetHistFromMap("KR_eventType_Truth_fid")->Fill(2);
              KR_truthKR_fid += 1;
            }
            if (tPU){
              m_hists->GetHistFromMap("KR_eventType_Truth_fid")->Fill(3);
              KR_truthPU_fid += 1;              
            }
            if (tO){
              m_hists->GetHistFromMap("KR_eventType_Truth_fid")->Fill(4);
              KR_truthO_fid += 1;                
            }            
            
            if (!m_cuts->mdc3()->SkinVeto(s1Start) && !m_cuts->mdc3()->ODVeto(s1Start)){
                nKr_veto += 1; 
                m_hists->GetHistFromMap("kr_s1_s2")->Fill(s1c, s2area);
                if (tSS){
                    m_hists->GetHistFromMap("KR_eventType_Truth_veto")->Fill(0);
                    KR_truthSS_veto += 1;
                    m_hists->GetHistFromMap("kr_Tss_s1_s2")->Fill(s1c, s2area);
                }
                if (tMS){
                    m_hists->GetHistFromMap("KR_eventType_Truth_veto")->Fill(1);
                    KR_truthMS_veto += 1;            
                }
                if (tKr){
                    m_hists->GetHistFromMap("KR_eventType_Truth_veto")->Fill(2);
                    KR_truthKR_veto += 1;
                }
                if (tPU){
                    m_hists->GetHistFromMap("KR_eventType_Truth_veto")->Fill(3);
                    KR_truthPU_veto += 1;              
                }
                if (tO){
                    m_hists->GetHistFromMap("KR_eventType_Truth_veto")->Fill(4);
                    KR_truthO_veto += 1;               
                }      
              if(s1c < 100){ 
                KR_100phd += 1;
                if (tSS) KR_truthSS_100phd += 1;
                if (tMS) KR_truthMS_100phd += 1;
                if (tKr) KR_truthKR_100phd += 1;
                if (tPU) KR_truthPU_100phd += 1;
                if (tO)  KR_truthO_100phd += 1;
              }
              if(s1c > 100 && s1c < 500){
                KR_100to500phd += 1;
                if (tSS) KR_truthSS_100to500phd += 1;
                if (tMS) KR_truthMS_100to500phd += 1;
                if (tKr) KR_truthKR_100to500phd += 1;
                if (tPU) KR_truthPU_100to500phd += 1;
                if (tO)  KR_truthO_100to500phd += 1;
              }
              if(s1c > 500 && s1c < 1000){
                KR_500to1000phd += 1;
                if (tSS) KR_truthSS_500to1000phd += 1;
                if (tMS) KR_truthMS_500to1000phd += 1;
                if (tKr) KR_truthKR_500to1000phd += 1;
                if (tPU) KR_truthPU_500to1000phd += 1;
                if (tO)  KR_truthO_500to1000phd += 1;
              }
              if(s1c > 1000 && s1c < 1500){
                KR_1000to1500phd += 1;
                if (tSS) KR_truthSS_1000to1500phd += 1;
                if (tMS) KR_truthMS_1000to1500phd += 1;
                if (tKr) KR_truthKR_1000to1500phd += 1;
                if (tPU) KR_truthPU_1000to1500phd += 1;
                if (tO)  KR_truthO_1000to1500phd += 1;
              }
              if(s1c > 1500){
                KR_1500phd += 1;  
                if (tSS) KR_truthSS_1500phd += 1;
                if (tMS) KR_truthMS_1500phd += 1;
                if (tKr) KR_truthKR_1500phd += 1;
                if (tPU) KR_truthPU_1500phd += 1;
                if (tO)  KR_truthO_1500phd += 1;
               }                 
            }
        }  
    }

    

    m_hists->GetHistFromMap("KR_TruthRawS1VsS1area")->Fill(totalTruthRawS1s, s1c);
    m_hists->GetHistFromMap("KR_TruthHitS1VsS1area")->Fill(totalTruthHitS1s, s1c);
    m_hists->GetHistFromMap("KR_TruthDetectedS1VsS1area")->Fill(totalTruthDetectedS1s, s1c);
    m_hists->GetHistFromMap("KR_TruthDetectedS1VsTruthRaw")->Fill(totalTruthDetectedS1s, totalTruthRawS1s);

    float fractionOfRawToS1 = float(s1c)/float(totalTruthRawS1s);
    float fractionOfHitToS1 = float(s1c)/float(totalTruthHitS1s);
    float fractionOfDetectedToS1 = float(s1c)/float(totalTruthDetectedS1s);
    float DetectedDivRaw = float(totalTruthDetectedS1s)/float(totalTruthRawS1s);

    m_hists->GetHistFromMap("KR_DetectedDivRaw")->Fill(DetectedDivRaw);
    m_hists->GetHistFromMap("KR_S1areaDivRawS1")->Fill(fractionOfRawToS1);
    m_hists->GetHistFromMap("KR_S1areaDivHitS1")->Fill(fractionOfHitToS1);
    m_hists->GetHistFromMap("KR_S1areaDivDetectedS1")->Fill(fractionOfDetectedToS1);     
  } // end Kr

  //PU
  if (numPU > 0){
    nPU += 1;     
    int nS1s = (*m_event->m_pileupScatter)->nS1;
    int nS2s = (*m_event->m_pileupScatter)->nS2;
    vector<int> s1pulseIDs = (*m_event->m_pileupScatter)->s1PulseIDs;
    vector<pair<int, float>> s1pulses;
    vector<int> s2pulseIDs = (*m_event->m_pileupScatter)->s2PulseIDs;
    vector<pair<int, float>> s2pulses;

    //pair the S1pulses to their respective areas
    for (int n=0; n<s1pulseIDs.size(); n++) {
          s1pulses.push_back(make_pair(s1pulseIDs[n], (*m_event->m_tpcPulses)->pulseArea_phd[s1pulseIDs[n]]));
        }
    sort(s1pulses.begin(), s1pulses.end(), CutsMDC3_MCTruthEfficiency::sortinrev);

    //pair the S2pulses to their respective areas
    for (int n=0; n<s2pulseIDs.size(); n++) {
          s2pulses.push_back(make_pair(s2pulseIDs[n], (*m_event->m_tpcPulses)->pulseArea_phd[s2pulseIDs[n]]));
        }
        sort(s2pulses.begin(), s2pulses.end(), CutsMDC3_MCTruthEfficiency::sortinrev);
      
    float s1Start = (*m_event->m_tpcPulses)->pulseStartTime_ns[s1pulses[0].first] + (*m_event->m_tpcPulses)->areaFractionTime1_ns[s1pulses[0].first];
    float s2Start = (*m_event->m_tpcPulses)->pulseStartTime_ns[s2pulses[0].first] + (*m_event->m_tpcPulses)->areaFractionTime1_ns[s2pulses[0].first];      
    float drift = abs(s2Start - s1Start);
    float radius = (*m_event->m_tpcPulses)->s2Xposition_cm[s2pulses[0].first]*1.e2 + (*m_event->m_tpcPulses)->s2Yposition_cm[s2pulses[0].first]*1.e2;

    if (!m_cuts->mdc3()->ETrainVeto(s2pulses[0].first)){
        m_hists->GetHistFromMap("eventType_et")->Fill(3);
        nPU_et += 1;  
        if (tSS){
           m_hists->GetHistFromMap("PU_eventType_Truth_et")->Fill(0);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(0);
           PU_truthSS_et += 1;
           PU_tSS_et = true;
        }
        if (tMS){
           m_hists->GetHistFromMap("PU_eventType_Truth_et")->Fill(1);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(1);
           PU_truthMS_et += 1;                
        }
        if (tKr){
           m_hists->GetHistFromMap("PU_eventType_Truth_et")->Fill(2);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(2);
           PU_truthKR_et += 1;
        }
        if (tPU){
           m_hists->GetHistFromMap("PU_eventType_Truth_et")->Fill(3);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(3);
           PU_truthPU_et += 1;              
        }
        if (tO){
           m_hists->GetHistFromMap("PU_eventType_Truth_et")->Fill(4);
           m_hists->GetHistFromMap("eventType_Truth_et")->Fill(4);
           PU_truthO_et += 1;                
        }
        if (m_cuts->mdc3()->Fiducial(radius,drift)){
            nPU_fid += 1;
            if (tSS){ 
              m_hists->GetHistFromMap("PU_eventType_Truth_fid")->Fill(0);
              PU_truthSS_fid += 1;
            }
            if (tMS){
              m_hists->GetHistFromMap("PU_eventType_Truth_fid")->Fill(1);
              PU_truthMS_fid += 1;            
            }
            if (tKr){
              m_hists->GetHistFromMap("PU_eventType_Truth_fid")->Fill(2);
              PU_truthKR_fid += 1;
            }
            if (tPU){
              m_hists->GetHistFromMap("PU_eventType_Truth_fid")->Fill(3);
              PU_truthPU_fid += 1;              
            }
            if (tO){
              m_hists->GetHistFromMap("PU_eventType_Truth_fid")->Fill(4);
              PU_truthO_fid += 1;                
            }
            if (!m_cuts->mdc3()->SkinVeto(s1Start) && !m_cuts->mdc3()->ODVeto(s1Start)){
                nPU_veto += 1; 
                m_hists->GetHistFromMap("pu_s1_s2")->Fill(s1pulses[0].second, s2pulses[0].second);
                
                if (tSS){
                    m_hists->GetHistFromMap("PU_eventType_Truth_veto")->Fill(0);
                    PU_truthSS_veto += 1;
                    m_hists->GetHistFromMap("pu_Tss_s1_s2")->Fill(s1pulses[0].second, s2pulses[0].second);
                }
                if (tMS){
                    m_hists->GetHistFromMap("PU_eventType_Truth_veto")->Fill(1);
                    PU_truthMS_veto += 1;            
                }
                if (tKr){
                    m_hists->GetHistFromMap("PU_eventType_Truth_veto")->Fill(2);
                    PU_truthKR_veto += 1;
                }
                if (tPU){
                    m_hists->GetHistFromMap("PU_eventType_Truth_veto")->Fill(3);
                    PU_truthPU_veto += 1;              
                }
                if (tO){
                    m_hists->GetHistFromMap("PU_eventType_Truth_veto")->Fill(4);
                    PU_truthO_veto += 1;               
                }
              if(s1pulses[0].second < 100){ 
                PU_100phd += 1;
                if (tSS) PU_truthSS_100phd += 1;                   
                if (tMS) PU_truthMS_100phd += 1;
                if (tKr) PU_truthKR_100phd += 1;
                if (tPU) PU_truthPU_100phd += 1;
                if (tO)  PU_truthO_100phd += 1;
              }
              if(s1pulses[0].second > 100 && s1pulses[0].second < 500){
                PU_100to500phd += 1;
                if (tSS) PU_truthSS_100to500phd += 1;
                if (tMS) PU_truthMS_100to500phd += 1;
                if (tKr) PU_truthKR_100to500phd += 1;
                if (tPU) PU_truthPU_100to500phd += 1;
                if (tO)  PU_truthO_100to500phd += 1;
              }
              if(s1pulses[0].second > 500 && s1pulses[0].second < 1000){
                PU_500to1000phd += 1;
                if (tSS) PU_truthSS_500to1000phd += 1;
                if (tMS) PU_truthMS_500to1000phd += 1;
                if (tKr) PU_truthKR_500to1000phd += 1;
                if (tPU) PU_truthPU_500to1000phd += 1;
                if (tO)  PU_truthO_500to1000phd += 1;
              }
              if(s1pulses[0].second > 1000 && s1pulses[0].second < 1500){
                PU_1000to1500phd += 1;
                if (tSS) PU_truthSS_1000to1500phd += 1;
                if (tMS) PU_truthMS_1000to1500phd += 1;
                if (tKr) PU_truthKR_1000to1500phd += 1;
                if (tPU) PU_truthPU_1000to1500phd += 1;
                if (tO)  PU_truthO_1000to1500phd += 1;
              }
              if(s1pulses[0].second > 1500){
                PU_1500phd += 1;  
                if (tSS) PU_truthSS_1500phd += 1;
                if (tMS) PU_truthMS_1500phd += 1;
                if (tKr) PU_truthKR_1500phd += 1;
                if (tPU) PU_truthPU_1500phd += 1;
                if (tO)  PU_truthO_1500phd += 1;
               }
            } 
        }
    }             
            

    m_hists->GetHistFromMap("PU_TruthRawS1VsS1area")->Fill(totalTruthRawS1s, s1pulses[0].second);
    m_hists->GetHistFromMap("PU_TruthHitS1VsS1area")->Fill(totalTruthHitS1s, s1pulses[0].second);
    m_hists->GetHistFromMap("PU_TruthDetectedS1VsS1area")->Fill(totalTruthDetectedS1s, s1pulses[0].second);
    m_hists->GetHistFromMap("PU_TruthDetectedS1VsTruthRaw")->Fill(totalTruthDetectedS1s, totalTruthRawS1s);

    float fractionOfRawToS1 = float(s1pulses[0].second)/float(totalTruthRawS1s);
    float fractionOfHitToS1 = float(s1pulses[0].second)/float(totalTruthHitS1s);
    float fractionOfDetectedToS1 = float(s1pulses[0].second)/float(totalTruthDetectedS1s);
    float DetectedDivRaw = float(totalTruthDetectedS1s)/float(totalTruthRawS1s);

    m_hists->GetHistFromMap("PU_DetectedDivRaw")->Fill(DetectedDivRaw);
    m_hists->GetHistFromMap("PU_S1areaDivRawS1")->Fill(fractionOfRawToS1);
    m_hists->GetHistFromMap("PU_S1areaDivHitS1")->Fill(fractionOfHitToS1);
    m_hists->GetHistFromMap("PU_S1areaDivDetectedS1")->Fill(fractionOfDetectedToS1);
  } 
  
  //end PU
  //Other
  if (numOther > 0){
    nOther += 1;    
    int nS1s = (*m_event->m_otherScatter)->nS1s;
    int nS2s = (*m_event->m_otherScatter)->nS2s;
    vector<int> s1pulseIDs = (*m_event->m_otherScatter)->s1PulseIDs;
    vector<pair<int, float>> s1pulses;
    vector<int> s2pulseIDs = (*m_event->m_otherScatter)->s2PulseIDs;
    vector<pair<int, float>> s2pulses;

    //pair the S1pulses to their respective areas
    for (int n=0; n<s1pulseIDs.size(); n++) {
          s1pulses.push_back(make_pair(s1pulseIDs[n], (*m_event->m_tpcPulses)->pulseArea_phd[s1pulseIDs[n]]));
        }
    sort(s1pulses.begin(), s1pulses.end(), CutsMDC3_MCTruthEfficiency::sortinrev);

    //pair the S2pulses to their respective areas
    for (int n=0; n<s2pulseIDs.size(); n++) {
          s2pulses.push_back(make_pair(s2pulseIDs[n], (*m_event->m_tpcPulses)->pulseArea_phd[s2pulseIDs[n]]));
        }
        sort(s2pulses.begin(), s2pulses.end(), CutsMDC3_MCTruthEfficiency::sortinrev);

    if (nS1s >0 && nS2s > 0){      
        float s1Start = (*m_event->m_tpcPulses)->pulseStartTime_ns[s1pulses[0].first] + (*m_event->m_tpcPulses)->areaFractionTime1_ns[s1pulses[0].first];
        float s2Start = (*m_event->m_tpcPulses)->pulseStartTime_ns[s2pulses[0].first] + (*m_event->m_tpcPulses)->areaFractionTime1_ns[s2pulses[0].first];     
        float drift = abs(s2Start - s1Start);
        float radius = (*m_event->m_tpcPulses)->s2Xposition_cm[s2pulses[0].first]*1.e2 + (*m_event->m_tpcPulses)->s2Yposition_cm[s2pulses[0].first]*1.e2;      
      

        if (!m_cuts->mdc3()->ETrainVeto(s2pulses[0].first)){
            m_hists->GetHistFromMap("eventType_et")->Fill(4);
            nOther_et += 1;
            if (tSS){ 
               m_hists->GetHistFromMap("OTHER_eventType_Truth_et")->Fill(0);
               m_hists->GetHistFromMap("eventType_Truth_et")->Fill(0);
               O_truthSS_et += 1;
               O_tSS_et = true;
            }
            if (tMS){
               m_hists->GetHistFromMap("OTHER_eventType_Truth_et")->Fill(1);
               m_hists->GetHistFromMap("eventType_Truth_et")->Fill(1);
               O_truthMS_et += 1;                
            }
            if (tKr){
               m_hists->GetHistFromMap("OTHER_eventType_Truth_et")->Fill(2);
               m_hists->GetHistFromMap("eventType_Truth_et")->Fill(2);
               O_truthKR_et += 1;
            }
            if (tPU){
               m_hists->GetHistFromMap("OTHER_eventType_Truth_et")->Fill(3);
               m_hists->GetHistFromMap("eventType_Truth_et")->Fill(3);
               O_truthPU_et += 1;              
            }
            if (tO){
               m_hists->GetHistFromMap("OTHER_eventType_Truth_et")->Fill(4);
               m_hists->GetHistFromMap("eventType_Truth_et")->Fill(4);
               O_truthO_et += 1;                
            }
            if (m_cuts->mdc3()->Fiducial(radius,drift)){
                nOther_fid += 1;
                if (tSS){ 
                  m_hists->GetHistFromMap("OTHER_eventType_Truth_fid")->Fill(0);
                  O_truthSS_fid += 1;
                }
                if (tMS){
                  m_hists->GetHistFromMap("OTHER_eventType_Truth_fid")->Fill(1);
                  O_truthMS_fid += 1;            
                }
                if (tKr){
                  m_hists->GetHistFromMap("OTHER_eventType_Truth_fid")->Fill(2);
                  O_truthKR_fid += 1;
                }
                if (tPU){
                  m_hists->GetHistFromMap("OTHER_eventType_Truth_fid")->Fill(3);
                  O_truthPU_fid += 1;              
                }
                if (tO){
                  m_hists->GetHistFromMap("OTHER_eventType_Truth_fid")->Fill(4);
                  O_truthO_fid += 1;                
                }
                if (!m_cuts->mdc3()->SkinVeto(s1Start) && !m_cuts->mdc3()->ODVeto(s1Start)){
                     m_hists->GetHistFromMap("other_s1_s2")->Fill(s1pulses[0].second, s2pulses[0].second);
                     nOther_veto += 1;
                    if (tSS){
                        m_hists->GetHistFromMap("OTHER_eventType_Truth_veto")->Fill(0);
                        O_truthSS_veto += 1;
                    }
                    if (tMS){
                        m_hists->GetHistFromMap("OTHER_eventType_Truth_veto")->Fill(1);
                        O_truthMS_veto += 1;            
                    }
                    if (tKr){
                        m_hists->GetHistFromMap("OTHER_eventType_Truth_veto")->Fill(2);
                        O_truthKR_veto += 1;
                    }
                    if (tPU){
                        m_hists->GetHistFromMap("OTHER_eventType_Truth_veto")->Fill(3);
                        O_truthPU_veto += 1;              
                    }
                    if (tO){
                        m_hists->GetHistFromMap("OTHER_eventType_Truth_veto")->Fill(4);
                        O_truthO_veto += 1;               
                    }                                    
                      if(s1pulses[0].second < 100){ 
                        O_100phd += 1;
                        if (tSS) O_truthSS_100phd += 1;
                        if (tMS) O_truthMS_100phd += 1;
                        if (tKr) O_truthKR_100phd += 1;
                        if (tPU) O_truthPU_100phd += 1;
                        if (tO)  O_truthO_100phd += 1;
                      }
                      if(s1pulses[0].second > 100 && s1pulses[0].second < 500){
                        O_100to500phd += 1;
                        if (tSS) O_truthSS_100to500phd += 1;
                        if (tMS) O_truthMS_100to500phd += 1;
                        if (tKr) O_truthKR_100to500phd += 1;
                        if (tPU) O_truthPU_100to500phd += 1;
                        if (tO)  O_truthO_100to500phd += 1;
                      }
                      if(s1pulses[0].second > 500 && s1pulses[0].second < 1000){
                        O_500to1000phd += 1;
                        if (tSS) O_truthSS_500to1000phd += 1;
                        if (tMS) O_truthMS_500to1000phd += 1;
                        if (tKr) O_truthKR_500to1000phd += 1;
                        if (tPU) O_truthPU_500to1000phd += 1;
                        if (tO)  O_truthO_500to1000phd += 1;
                      }
                      if(s1pulses[0].second > 1000 && s1pulses[0].second < 1500){
                        O_1000to1500phd += 1;
                        if (tSS) O_truthSS_1000to1500phd += 1;
                        if (tMS) O_truthMS_1000to1500phd += 1;
                        if (tKr) O_truthKR_1000to1500phd += 1;
                        if (tPU) O_truthPU_1000to1500phd += 1;
                        if (tO)  O_truthO_1000to1500phd += 1;
                      }
                      if(s1pulses[0].second > 1500){
                        O_1500phd += 1;  
                        if (tSS) O_truthSS_1500phd += 1;
                        if (tMS) O_truthMS_1500phd += 1;
                        if (tKr) O_truthKR_1500phd += 1;
                        if (tPU) O_truthPU_1500phd += 1;
                        if (tO)  O_truthO_1500phd += 1;
                       }
                }
            } 
        }  
            
        m_hists->GetHistFromMap("OTHER_TruthRawS1VsS1area")->Fill(totalTruthRawS1s, s1pulses[0].second);
        m_hists->GetHistFromMap("OTHER_TruthHitS1VsS1area")->Fill(totalTruthHitS1s, s1pulses[0].second);
        m_hists->GetHistFromMap("OTHER_TruthDetectedS1VsS1area")->Fill(totalTruthDetectedS1s, s1pulses[0].second);
        m_hists->GetHistFromMap("OTHER_TruthDetectedS1VsTruthRaw")->Fill(totalTruthDetectedS1s, totalTruthRawS1s);

        float fractionOfRawToS1 = float(s1pulses[0].second)/float(totalTruthRawS1s);
        float fractionOfHitToS1 = float(s1pulses[0].second)/float(totalTruthHitS1s);
        float fractionOfDetectedToS1 = float(s1pulses[0].second)/float(totalTruthDetectedS1s);
        float DetectedDivRaw = float(totalTruthDetectedS1s)/float(totalTruthRawS1s);

        m_hists->GetHistFromMap("OTHER_DetectedDivRaw")->Fill(DetectedDivRaw);
        m_hists->GetHistFromMap("OTHER_S1areaDivRawS1")->Fill(fractionOfRawToS1);
        m_hists->GetHistFromMap("OTHER_S1areaDivHitS1")->Fill(fractionOfHitToS1);
        m_hists->GetHistFromMap("OTHER_S1areaDivDetectedS1")->Fill(fractionOfDetectedToS1);
        
      }             
      
    if ( (*m_event->m_otherScatter)->nS1s != 0 && (*m_event->m_otherScatter)->nS2s != 0 ) m_hists->GetHistFromMap("otherType")->Fill(0);
    if ( (*m_event->m_otherScatter)->nS1s == 0 && (*m_event->m_otherScatter)->nS2s != 0 )
    {
        m_hists->GetHistFromMap("otherType")->Fill(1);  
        if (tSS){
            O_truthSS_1S1 += 1;
        }
        if (tMS){
            O_truthMS_1S1 += 1;
        }
        if (tKr){
            O_truthKR_1S1 += 1;
        }
        if (tPU){
            O_truthPU_1S1 += 1;
        }
        if (tO){
            O_truthO_1S1 += 1;
        }
    } 
    if ( (*m_event->m_otherScatter)->nS1s != 0 && (*m_event->m_otherScatter)->nS2s == 0 ) 
    {
        m_hists->GetHistFromMap("otherType")->Fill(2); 
        if (tSS){
            O_truthSS_1S2 += 1;
        }
        if (tMS){
            O_truthMS_1S2 += 1;
        }
        if (tKr){
            O_truthKR_1S2 += 1;
        }
        if (tPU){
            O_truthPU_1S2 += 1;
        }
        if (tO){
            O_truthO_1S2 += 1;
        }
    } 
    if ( (*m_event->m_otherScatter)->nS1s == 0 && (*m_event->m_otherScatter)->nS2s == 0 && (*m_event->m_tpcPulses)->nPulses != 0 ) m_hists->GetHistFromMap("otherType")->Fill(3);
    if ( (*m_event->m_otherScatter)->nS1s == 0 && (*m_event->m_otherScatter)->nS2s == 0 && (*m_event->m_tpcPulses)->nPulses == 0 ) m_hists->GetHistFromMap("otherType")->Fill(4);      
  } //end Other      
    
  vector<int> promS1s = (*m_event->m_tpcPulses)->s1PulseIDsByArea;  
  vector<int> promS2s = (*m_event->m_tpcPulses)->s2PulseIDsByArea;
  if (promS1s.size() > 0 && promS2s.size() > 0) { // make sure there is is least 1 S1 and 1 S2 in the event
      float leadingS1area = (*m_event->m_tpcPulses)->pulseArea_phd[promS1s[0]];
      float leadingS2area = (*m_event->m_tpcPulses)->pulseArea_phd[promS2s[0]];

      // evaluate ETrain cut only once
      bool S1ETrainVeto = false;
      if (promS1s[0] > -1) S1ETrainVeto = m_cuts->mdc3()->ETrainVeto(promS1s[0]);
      else if (promS1s[0] == -1 && (*m_event->m_tpcPulses)->nPulses != 0) S1ETrainVeto = m_cuts->mdc3()->ETrainVeto(0);
      else S1ETrainVeto = true;    
    
  // Make the all events histograms
    if (leadingS1area < 1500) {
    m_hists->GetHistFromMap("all_s1_s2")->Fill(leadingS1area, leadingS2area); 
    if (S1ETrainVeto) m_hists->GetHistFromMap("all_net_s1_s2")->Fill(leadingS1area, leadingS2area);
    if (SS_tMS_v || SS_tKR_et || SS_tPU_et || MS_tSS_v || O_tSS_et || KR_tSS_et || PU_tSS_et){
        m_hists->GetHistFromMap("all_s1_s2_missed")->Fill(leadingS1area, leadingS2area);
        if (S1ETrainVeto) m_hists->GetHistFromMap("all_net_s1_s2_missed")->Fill(leadingS2area, leadingS2area);
    }
    if (!S1ETrainVeto) {
	
      m_hists->GetHistFromMap("all_et_s1_s2")->Fill(leadingS1area, leadingS2area);
        if (SS_tMS_v || SS_tKR_et || SS_tPU_et || MS_tSS_v || O_tSS_et || PU_tSS_et || KR_tSS_et){
            m_hists->GetHistFromMap("all_et_s1_s2_missed")->Fill(leadingS1area, leadingS2area);
        }    
        
      if (promS1s.size() > 1) { //make sure there are second pulses to compare against...
        float subleadingS1area = (*m_event->m_tpcPulses)->pulseArea_phd[promS1s[1]];
        float subleadingS1height = (*m_event->m_tpcPulses)->peakAmp[promS1s[1]];
        float subleadingS1width = ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[promS1s[1]] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[promS1s[1]] );
        float h2wS1 = subleadingS1height/subleadingS1width;
        float s1ratio = subleadingS1area/leadingS1area;
        m_hists->GetHistFromMap("all_et_s1ratio_h2w_firstS1")->Fill(s1ratio, h2wS1);
        if (SS_tMS_v || SS_tKR_et || SS_tPU_et || MS_tSS_v || O_tSS_et || PU_tSS_et || KR_tSS_et){
            m_hists->GetHistFromMap("all_et_s1ratio_h2w_firstS1_missed")->Fill(s1ratio, h2wS1);
        }
          
        for (int n=1; n<promS1s.size(); n++) {
          float H2WS1 = (*m_event->m_tpcPulses)->peakAmp[promS1s[n]] / ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[promS1s[n]] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[promS1s[n]] );
          float S1ratio = (*m_event->m_tpcPulses)->pulseArea_phd[promS1s[n]]/ leadingS1area;
          m_hists->GetHistFromMap("all_et_s1ratio_h2w_allS1s")->Fill(S1ratio, H2WS1);
            if (SS_tMS_v || SS_tKR_et || SS_tPU_et || MS_tSS_v || O_tSS_et || PU_tSS_et || KR_tSS_et){
                m_hists->GetHistFromMap("all_et_s1ratio_h2w_allS1s_missed")->Fill(S1ratio, H2WS1);
            }
	      }
      } // end S1s    
        
      if (promS2s.size() > 1) {
        float subleadingS2area = (*m_event->m_tpcPulses)->pulseArea_phd[promS2s[1]];    
        float subleadingS2height = (*m_event->m_tpcPulses)->peakAmp[promS2s[1]];  
        float subleadingS2width = ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[promS2s[1]] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[promS2s[1]] );
        float h2wS2 = subleadingS2height/subleadingS2width;
        float s2ratio = subleadingS2area/leadingS2area;
        m_hists->GetHistFromMap("all_et_s2ratio_h2w_firstS2")->Fill(s2ratio, h2wS2);
        if (SS_tMS_v || SS_tKR_et || SS_tPU_et || MS_tSS_v || O_tSS_et || PU_tSS_et || KR_tSS_et){
            m_hists->GetHistFromMap("all_et_s2ratio_h2w_firstS2_missed")->Fill(s2ratio, h2wS2);
        }
          
        for (int m=1; m<promS2s.size(); m++) {
          float H2WS2 = (*m_event->m_tpcPulses)->peakAmp[promS2s[m]] / ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[promS2s[m]] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[promS2s[m]] );
          float S2ratio = (*m_event->m_tpcPulses)->pulseArea_phd[promS2s[m]]/ leadingS2area;
          m_hists->GetHistFromMap("all_et_s2ratio_h2w_allS2s")->Fill(S2ratio, H2WS2);
            if (SS_tMS_v || SS_tKR_et || SS_tPU_et || MS_tSS_v || O_tSS_et || PU_tSS_et || KR_tSS_et){
                m_hists->GetHistFromMap("all_et_s2ratio_h2w_allS2s_missed")->Fill(S2ratio, H2WS2);
            }
        }
      }// end S2s
    } // etrain veto
   } // end all events  
  } // end 1500 phd cut
/*   
// Get the max and sub S1s and S2s
  pair<vector<pair<int,float>>, vector<pair<int,float>>> allSXs = m_cutsMDC3_MCTruthEfficiency->GetAllSXs(); // goal is to run through all pulses only once
  vector<pair<int,float>> allS1s = allSXs.first;
  vector<pair<int,float>> allS2s = allSXs.second;
  int   maxS1pulseID = allS1s[0].first;
  float maxS1area    = allS1s[0].second;
  int   maxS2pulseID = allS2s[0].first;
  float maxS2area    = allS2s[0].second;
  
  // evaluate ETrain cut only once
  bool S1ETrainVeto = false;
  if (maxS1pulseID > -1) S1ETrainVeto = m_cuts->mdc3()->ETrainVeto(maxS1pulseID);
  else if (maxS1pulseID == -1 && (*m_event->m_tpcPulses)->nPulses != 0) S1ETrainVeto = m_cuts->mdc3()->ETrainVeto(0);
  else S1ETrainVeto = true;    

    
  // Make the all events histograms
  if (maxS1pulseID > -1 && maxS2pulseID > -1) { // make sure there is is least 1 S1 and 1 S2 in the event
    if (maxS1area < 1500) {
    m_hists->GetHistFromMap("all_s1_s2")->Fill(maxS1area, maxS2area); 
    if (S1ETrainVeto) m_hists->GetHistFromMap("all_net_s1_s2")->Fill(maxS1area, maxS2area);
    if (SS_tMS_v || SS_tKR_et || SS_tPU_et || MS_tSS_v || O_tSS_et || KR_tSS_et || PU_tSS_et){
        m_hists->GetHistFromMap("all_s1_s2_missed")->Fill(maxS1area, maxS2area);
        if (S1ETrainVeto) m_hists->GetHistFromMap("all_net_s1_s2_missed")->Fill(maxS1area, maxS2area);
    }
    if (!S1ETrainVeto) {
	
      m_hists->GetHistFromMap("all_et_s1_s2")->Fill(maxS1area, maxS2area);
        if (SS_tMS_v || SS_tKR_et || SS_tPU_et || MS_tSS_v || O_tSS_et || PU_tSS_et || KR_tSS_et){
            m_hists->GetHistFromMap("all_et_s1_s2_missed")->Fill(maxS1area, maxS2area);
        }
        
      if (allS1s.size() > 1) { //make sure there are second pulses to compare against...
        float H2WS1 = (*m_event->m_tpcPulses)->peakAmp[allS1s[1].first] / ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[allS1s[1].first] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[allS1s[1].first] );
        float S1ratio = allS1s[1].second / maxS1area;
        m_hists->GetHistFromMap("all_et_s1ratio_h2w_firstS1")->Fill(S1ratio, H2WS1);
        if (SS_tMS_v || SS_tKR_et || SS_tPU_et || MS_tSS_v || O_tSS_et || PU_tSS_et || KR_tSS_et){
            m_hists->GetHistFromMap("all_et_s1ratio_h2w_firstS1_missed")->Fill(S1ratio, H2WS1);
        }
          
        for (int n=1; n<allS1s.size(); n++) {
          H2WS1 = (*m_event->m_tpcPulses)->peakAmp[allS1s[n].first] / ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[allS1s[n].first] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[allS1s[n].first] );
          S1ratio = allS1s[n].second / maxS1area;
          m_hists->GetHistFromMap("all_et_s1ratio_h2w_allS1s")->Fill(S1ratio, H2WS1);
            if (SS_tMS_v || SS_tKR_et || SS_tPU_et || MS_tSS_v || O_tSS_et || PU_tSS_et || KR_tSS_et){
                m_hists->GetHistFromMap("all_et_s1ratio_h2w_allS1s_missed")->Fill(S1ratio, H2WS1);
            }
	      }
      } // end S1s

      if (allS2s.size() > 1) {
        float H2WS2 = (*m_event->m_tpcPulses)->peakAmp[allS2s[1].first] / ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[allS2s[1].first] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[allS2s[1].first] );
        float S2ratio = allS2s[1].second / maxS2area;
        m_hists->GetHistFromMap("all_et_s2ratio_h2w_firstS2")->Fill(S2ratio, H2WS2);
        if (SS_tMS_v || SS_tKR_et || SS_tPU_et || MS_tSS_v || O_tSS_et || PU_tSS_et || KR_tSS_et){
            m_hists->GetHistFromMap("all_et_s2ratio_h2w_firstS2_missed")->Fill(S2ratio, H2WS2);
        }
          
        for (int m=1; m<allS2s.size(); m++) {
          H2WS2 = (*m_event->m_tpcPulses)->peakAmp[allS2s[m].first] / ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[allS2s[m].first] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[allS2s[m].first] );
          S2ratio = allS2s[m].second / maxS2area;
          m_hists->GetHistFromMap("all_et_s2ratio_h2w_allS2s")->Fill(S2ratio, H2WS2);
            if (SS_tMS_v || SS_tKR_et || SS_tPU_et || MS_tSS_v || O_tSS_et || PU_tSS_et || KR_tSS_et){
                m_hists->GetHistFromMap("all_et_s2ratio_h2w_allS2s_missed")->Fill(S2ratio, H2WS2);
            }
        }
      }// end S2s
    } // etrain veto
   } // end all events  
  } // end 1500 phd cut
*/    
    
}  
    
  //std::cout << "end of event loop "  << std::endl;

// Finalize() - Called once after event loop.
void MDC3_MCTruthEfficiency::Finalize() {
  INFO("Finalizing MDC3_MCTruthEfficiency Analysis");
    
  // S1 cut lzap parameters - copied from LZap
  const float x1z = 1;
  const float x2z = 1.5e-3;
  const float y1z = 3e-5;
  const float y2z = 10; 
  const float logx1z = log10(x1z);
  const float logx2z = log10(x2z);
  const float logy1z = log10(y1z);
  const float logy2z = log10(y2z);
  const float m1z = (logy2z-logy1z)/(logx2z-logx1z);
  const float b1z = logy1z-m1z*logx1z;
    
  // S2 cut lzap parameters - copied from LZap
  const float x3z = 1;
  const float x4z = 1e-3;
  const float y3z = 3e-6;
  const float y4z = 10;
  const float logx3z = log10(x3z);
  const float logx4z = log10(x4z);
  const float logy3z = log10(y3z);
  const float logy4z = log10(y4z);
  const float m2z = (logy4z-logy3z)/(logx4z-logx3z);
  const float b2z = logy3z-m2z*logx3z;
    
  TH1* htmp = (TH1*)m_hists->GetHistFromMap("h_s1cut_lzap");
  TF1 *tf_s1cut_lzap = new TF1("tf_s1cut_lzap", "[0]*TMath::Power(x,[1])", x2z, x1z);
  tf_s1cut_lzap->SetParameters(pow(10,b1z),m1z);
  htmp->GetListOfFunctions()->Add(tf_s1cut_lzap);
    
  TH2* hnew = (TH2*)m_hists->GetHistFromMap("all_et_s1ratio_h2w_allS1s");
  hnew->GetListOfFunctions()->Add(tf_s1cut_lzap);

  hnew = (TH2*)m_hists->GetHistFromMap("all_et_s1ratio_h2w_allS1s_missed");
  hnew->GetListOfFunctions()->Add(tf_s1cut_lzap);
    
  hnew = (TH2*)m_hists->GetHistFromMap("all_et_s1ratio_h2w_firstS1");
  hnew->GetListOfFunctions()->Add(tf_s1cut_lzap);

  hnew = (TH2*)m_hists->GetHistFromMap("all_et_s1ratio_h2w_firstS1_missed");
  hnew->GetListOfFunctions()->Add(tf_s1cut_lzap);
    
  htmp = (TH1*)m_hists->GetHistFromMap("h_s2cut_lzap");
  TF1 *tf_s2cut_lzap = new TF1("tf_s2cut_lzap", "[0]*TMath::Power(x,[1])", x4z, x3z);
  tf_s2cut_lzap->SetParameters(pow(10,b2z),m2z);
  htmp->GetListOfFunctions()->Add(tf_s2cut_lzap);

  hnew = (TH2*)m_hists->GetHistFromMap("all_et_s2ratio_h2w_allS2s");
  hnew->GetListOfFunctions()->Add(tf_s2cut_lzap);

  hnew = (TH2*)m_hists->GetHistFromMap("all_et_s2ratio_h2w_allS2s_missed");
  hnew->GetListOfFunctions()->Add(tf_s2cut_lzap);
    
  hnew = (TH2*)m_hists->GetHistFromMap("all_et_s2ratio_h2w_firstS2");
  hnew->GetListOfFunctions()->Add(tf_s2cut_lzap);

  hnew = (TH2*)m_hists->GetHistFromMap("all_et_s2ratio_h2w_firstS2_missed");
  hnew->GetListOfFunctions()->Add(tf_s2cut_lzap);
    
  FILE *f = fopen("/global/homes/m/mrw7816/AlpacaAnalysis/run/MDC3_MCTruthEfficiency/cout.txt", "w");   
  fprintf(f, "SS_truthSS_et: %f, SS_truthSS_fid: %f, SS_truthSS_veto: %f, SS_truthSS_100phd: %f, SS_truthSS_100to500phd: %f, SS_truthSS_500to1000phd: %f, SS_truthSS_1000to1500phd: %f\n", SS_truthSS_et, SS_truthSS_fid, SS_truthSS_veto,SS_truthSS_100phd,SS_truthSS_100to500phd,SS_truthSS_500to1000phd,SS_truthSS_1000to1500phd);
  fprintf(f, "SS_truthMS_et: %f, SS_truthMS_fid: %f, SS_truthMS_veto: %f, SS_truthMS_100phd: %f, SS_truthMS_100to500phd: %f, SS_truthMS_500to1000phd: %f, SS_truthMS_1000to1500phd: %f\n", SS_truthMS_et, SS_truthMS_fid, SS_truthMS_veto,SS_truthMS_100phd,SS_truthMS_100to500phd,SS_truthMS_500to1000phd,SS_truthMS_1000to1500phd);    
  fprintf(f, "SS_truthKR_et: %f, SS_truthKR_fid: %f, SS_truthKR_veto: %f, SS_truthKR_100phd: %f, SS_truthKR_100to500phd: %f, SS_truthKR_500to1000phd: %f, SS_truthKR_1000to1500phd: %f\n", SS_truthKR_et, SS_truthKR_fid, SS_truthKR_veto,SS_truthKR_100phd,SS_truthKR_100to500phd,SS_truthKR_500to1000phd,SS_truthKR_1000to1500phd);
  fprintf(f, "SS_truthPU_et: %f, SS_truthPU_fid: %f, SS_truthPU_veto: %f, SS_truthPU_100phd: %f, SS_truthPU_100to500phd: %f, SS_truthPU_500to1000phd: %f, SS_truthPU_1000to1500phd: %f\n", SS_truthPU_et, SS_truthPU_fid, SS_truthPU_veto,SS_truthPU_100phd,SS_truthPU_100to500phd,SS_truthPU_500to1000phd,SS_truthPU_1000to1500phd);
  fprintf(f, "SS_truthO_et: %f, SS_truthO_fid: %f, SS_truthO_veto: %f, SS_truthO_100phd: %f, SS_truthO_100to500phd: %f, SS_truthO_500to1000phd: %f, SS_truthO_1000to1500phd: %f\n", SS_truthO_et, SS_truthO_fid, SS_truthO_veto,SS_truthO_100phd,SS_truthO_100to500phd,SS_truthO_500to1000phd,SS_truthO_1000to1500phd);

  fprintf(f, "MS_truthSS_et: %f, MS_truthSS_fid: %f, MS_truthSS_veto: %f, MS_truthSS_100phd: %f, MS_truthSS_100to500phd: %f, MS_truthSS_500to1000phd: %f, MS_truthSS_1000to1500phd: %f\n", MS_truthSS_et, MS_truthSS_fid, MS_truthSS_veto,MS_truthSS_100phd,MS_truthSS_100to500phd,MS_truthSS_500to1000phd,MS_truthSS_1000to1500phd);
  fprintf(f, "MS_truthMS_et: %f, MS_truthMS_fid: %f, MS_truthMS_veto: %f, MS_truthMS_100phd: %f, MS_truthMS_100to500phd: %f, MS_truthMS_500to1000phd: %f, MS_truthMS_1000to1500phd: %f\n", MS_truthMS_et, MS_truthMS_fid, MS_truthMS_veto,MS_truthMS_100phd,MS_truthMS_100to500phd,MS_truthMS_500to1000phd,MS_truthMS_1000to1500phd);    
  fprintf(f, "MS_truthKR_et: %f, MS_truthKR_fid: %f, MS_truthKR_veto: %f, MS_truthKR_100phd: %f, MS_truthKR_100to500phd: %f, MS_truthKR_500to1000phd: %f, MS_truthKR_1000to1500phd: %f\n", MS_truthKR_et, MS_truthKR_fid, MS_truthKR_veto,MS_truthKR_100phd,MS_truthKR_100to500phd,MS_truthKR_500to1000phd,MS_truthKR_1000to1500phd);
  fprintf(f, "MS_truthPU_et: %f, MS_truthPU_fid: %f, MS_truthPU_veto: %f, MS_truthPU_100phd: %f, MS_truthPU_100to500phd: %f, MS_truthPU_500to1000phd: %f, MS_truthPU_1000to1500phd: %f\n", MS_truthPU_et, MS_truthPU_fid, MS_truthPU_veto,MS_truthPU_100phd,MS_truthPU_100to500phd,MS_truthPU_500to1000phd,MS_truthPU_1000to1500phd);
  fprintf(f, "MS_truthO_et: %f, MS_truthO_fid: %f, MS_truthO_veto: %f, MS_truthO_100phd: %f, MS_truthO_100to500phd: %f, MS_truthO_500to1000phd: %f, MS_truthO_1000to1500phd: %f\n", MS_truthO_et, MS_truthO_fid, MS_truthO_veto,MS_truthO_100phd,MS_truthO_100to500phd,MS_truthO_500to1000phd,MS_truthO_1000to1500phd);    

  fprintf(f, "KR_truthSS_et: %f, KR_truthSS_fid: %f, KR_truthSS_veto: %f, KR_truthSS_100phd: %f, KR_truthSS_100to500phd: %f, KR_truthSS_500to1000phd: %f, KR_truthSS_1000to1500phd: %f\n", KR_truthSS_et, KR_truthSS_fid, KR_truthSS_veto,KR_truthSS_100phd,KR_truthSS_100to500phd,KR_truthSS_500to1000phd,KR_truthSS_1000to1500phd);
  fprintf(f, "KR_truthMS_et: %f, KR_truthMS_fid: %f, KR_truthMS_veto: %f, KR_truthMS_100phd: %f, KR_truthMS_100to500phd: %f, KR_truthMS_500to1000phd: %f, KR_truthMS_1000to1500phd: %f\n", KR_truthMS_et, KR_truthMS_fid, KR_truthMS_veto,KR_truthMS_100phd,KR_truthMS_100to500phd,KR_truthMS_500to1000phd,KR_truthMS_1000to1500phd);    
  fprintf(f, "KR_truthKR_et: %f, KR_truthKR_fid: %f, KR_truthKR_veto: %f, KR_truthKR_100phd: %f, KR_truthKR_100to500phd: %f, KR_truthKR_500to1000phd: %f, KR_truthKR_1000to1500phd: %f\n", KR_truthKR_et, KR_truthKR_fid, KR_truthKR_veto,KR_truthKR_100phd,KR_truthKR_100to500phd,KR_truthKR_500to1000phd,KR_truthKR_1000to1500phd);
  fprintf(f, "KR_truthPU_et: %f, KR_truthPU_fid: %f, KR_truthPU_veto: %f, KR_truthPU_100phd: %f, KR_truthPU_100to500phd: %f, KR_truthPU_500to1000phd: %f, KR_truthPU_1000to1500phd: %f\n", KR_truthPU_et, KR_truthPU_fid, KR_truthPU_veto,KR_truthPU_100phd,KR_truthPU_100to500phd,KR_truthPU_500to1000phd,KR_truthPU_1000to1500phd);
  fprintf(f, "KR_truthO_et: %f, KR_truthO_fid: %f, KR_truthO_veto: %f, KR_truthO_100phd: %f, KR_truthO_100to500phd: %f, KR_truthO_500to1000phd: %f, KR_truthO_1000to1500phd: %f\n", KR_truthO_et, KR_truthO_fid, KR_truthO_veto,KR_truthO_100phd,KR_truthO_100to500phd,KR_truthO_500to1000phd,KR_truthO_1000to1500phd);          

  fprintf(f, "PU_truthSS_et: %f, PU_truthSS_fid: %f, PU_truthSS_veto: %f, PU_truthSS_100phd: %f, PU_truthSS_100to500phd: %f, PU_truthSS_500to1000phd: %f, PU_truthSS_1000to1500phd: %f\n", PU_truthSS_et, PU_truthSS_fid, PU_truthSS_veto,PU_truthSS_100phd,PU_truthSS_100to500phd,PU_truthSS_500to1000phd,PU_truthSS_1000to1500phd);
  fprintf(f, "PU_truthMS_et: %f, PU_truthMS_fid: %f, PU_truthMS_veto: %f, PU_truthMS_100phd: %f, PU_truthMS_100to500phd: %f, PU_truthMS_500to1000phd: %f, PU_truthMS_1000to1500phd: %f\n", PU_truthMS_et, PU_truthMS_fid, PU_truthMS_veto,PU_truthMS_100phd,PU_truthMS_100to500phd,PU_truthMS_500to1000phd,PU_truthMS_1000to1500phd);    
  fprintf(f, "PU_truthKR_et: %f, PU_truthKR_fid: %f, PU_truthKR_veto: %f, PU_truthKR_100phd: %f, PU_truthKR_100to500phd: %f, PU_truthKR_500to1000phd: %f, PU_truthKR_1000to1500phd: %f\n", PU_truthKR_et, PU_truthKR_fid, PU_truthKR_veto,PU_truthKR_100phd,PU_truthKR_100to500phd,PU_truthKR_500to1000phd,PU_truthKR_1000to1500phd);
  fprintf(f, "PU_truthPU_et: %f, PU_truthPU_fid: %f, PU_truthPU_veto: %f, PU_truthPU_100phd: %f, PU_truthPU_100to500phd: %f, PU_truthPU_500to1000phd: %f, PU_truthPU_1000to1500phd: %f\n", PU_truthPU_et, PU_truthPU_fid, PU_truthPU_veto,PU_truthPU_100phd,PU_truthPU_100to500phd,PU_truthPU_500to1000phd,PU_truthPU_1000to1500phd);
  fprintf(f, "PU_truthO_et: %f, PU_truthO_fid: %f, PU_truthO_veto: %f, PU_truthO_100phd: %f, PU_truthO_100to500phd: %f, PU_truthO_500to1000phd: %f, PU_truthO_1000to1500phd: %f\n", PU_truthO_et, PU_truthO_fid, PU_truthO_veto,PU_truthO_100phd,PU_truthO_100to500phd,PU_truthO_500to1000phd,PU_truthO_1000to1500phd);    

  fprintf(f, "O_truthSS_et: %f, O_truthSS_fid: %f, O_truthSS_veto: %f, O_truthSS_100phd: %f, O_truthSS_100to500phd: %f, O_truthSS_500to1000phd: %f, O_truthSS_1000to1500phd: %f\n", O_truthSS_et, O_truthSS_fid, O_truthSS_veto,O_truthSS_100phd,O_truthSS_100to500phd,O_truthSS_500to1000phd,O_truthSS_1000to1500phd);
  fprintf(f, "O_truthMS_et: %f, O_truthMS_fid: %f, O_truthMS_veto: %f, O_truthMS_100phd: %f, O_truthMS_100to500phd: %f, O_truthMS_500to1000phd: %f, O_truthMS_1000to1500phd: %f\n", O_truthMS_et, O_truthMS_fid, O_truthMS_veto,O_truthMS_100phd,O_truthMS_100to500phd,O_truthMS_500to1000phd,O_truthMS_1000to1500phd);    
  fprintf(f, "O_truthKR_et: %f, O_truthKR_fid: %f, O_truthKR_veto: %f, O_truthKR_100phd: %f, O_truthKR_100to500phd: %f, O_truthKR_500to1000phd: %f, O_truthKR_1000to1500phd: %f\n", O_truthKR_et, O_truthKR_fid, O_truthKR_veto,O_truthKR_100phd,O_truthKR_100to500phd,O_truthKR_500to1000phd,O_truthKR_1000to1500phd);
  fprintf(f, "O_truthPU_et: %f, O_truthPU_fid: %f, O_truthPU_veto: %f, O_truthPU_100phd: %f, O_truthPU_100to500phd: %f, O_truthPU_500to1000phd: %f, O_truthPU_1000to1500phd: %f\n", O_truthPU_et, O_truthPU_fid, O_truthPU_veto,O_truthPU_100phd,O_truthPU_100to500phd,O_truthPU_500to1000phd,O_truthPU_1000to1500phd);
  fprintf(f, "O_truthO_et: %f, O_truthO_fid: %f, O_truthO_veto: %f, O_truthO_100phd: %f, O_truthO_100to500phd: %f, O_truthO_500to1000phd: %f, O_truthO_1000to1500phd: %f\n", O_truthO_et, O_truthO_fid, O_truthO_veto,O_truthO_100phd,O_truthO_100to500phd,O_truthO_500to1000phd,O_truthO_1000to1500phd);
   fprintf(f, "O_truthSS_1S1: %f, O_truthMS_1S1: %f, O_truthKR_1S1: %f, O_truthPU_1S1: %f, O_truthO_1S1: %f\n", O_truthSS_1S1, O_truthMS_1S1, O_truthKR_1S1,O_truthPU_1S1,O_truthO_1S1);
   fprintf(f, "O_truthSS_1S2: %f, O_truthMS_1S2: %f, O_truthKR_1S2: %f, O_truthPU_1S2: %f, O_truthO_1S2: %f\n", O_truthSS_1S2, O_truthMS_1S2, O_truthKR_1S2,O_truthPU_1S2,O_truthO_1S2);
    
  truthSS_et = SS_truthSS_et + MS_truthSS_et + KR_truthSS_et + PU_truthSS_et + O_truthSS_et;
  truthSS_fid = SS_truthSS_fid + MS_truthSS_fid + KR_truthSS_fid + PU_truthSS_fid + O_truthSS_fid;
  truthSS_veto = SS_truthSS_veto + MS_truthSS_veto + KR_truthSS_veto + PU_truthSS_veto + O_truthSS_veto;
    
  truthMS_et = SS_truthMS_et + MS_truthMS_et + KR_truthMS_et + PU_truthMS_et + O_truthMS_et;
  truthMS_fid = SS_truthMS_fid + MS_truthMS_fid + KR_truthMS_fid + PU_truthMS_fid + O_truthMS_fid;
  truthMS_veto = SS_truthMS_veto + MS_truthMS_veto + KR_truthMS_veto + PU_truthMS_veto + O_truthMS_veto;
    
  truthKr_et = SS_truthKR_et + MS_truthKR_et + KR_truthKR_et + PU_truthKR_et + O_truthKR_et;
  truthKr_fid = SS_truthKR_fid + MS_truthKR_fid + KR_truthKR_fid + PU_truthKR_fid + O_truthKR_fid;
  truthKr_veto = SS_truthKR_veto + MS_truthKR_veto + KR_truthKR_veto + PU_truthKR_veto + O_truthKR_veto;
    
  truthPU_et = SS_truthPU_et + MS_truthPU_et + KR_truthPU_et + PU_truthPU_et + O_truthPU_et;
  truthPU_fid = SS_truthPU_fid + MS_truthPU_fid + KR_truthPU_fid + PU_truthPU_fid + O_truthPU_fid;
  truthPU_veto = SS_truthPU_veto + MS_truthPU_veto + KR_truthPU_veto + PU_truthPU_veto + O_truthPU_veto;
    
  truthO_et = SS_truthO_et + MS_truthO_et + KR_truthO_et + PU_truthO_et + O_truthO_et;
  truthO_fid = SS_truthO_fid + MS_truthO_fid + KR_truthO_fid + PU_truthO_fid + O_truthO_fid;
  truthO_veto = SS_truthO_veto + MS_truthO_veto + KR_truthO_veto + PU_truthO_veto + O_truthO_veto;
      
  fprintf(f, "nSS_et: %f, truthSS_et: %f, SSl/t_et: %f\n", nSS_et, truthSS_et, nSS_et/truthSS_et);
  fprintf(f, "nSS_fid: %f, truthSS_fid: %f, SSl/t_fid: %f\n", nSS_fid, truthSS_fid, nSS_fid/truthSS_fid);
  fprintf(f, "nSS_veto: %f, truthSS_veto: %f, SSl/t_veto: %f\n", nSS_veto, truthSS_veto, nSS_veto/truthSS_veto);

  fprintf(f, "SS_100phd: %f, SS_100to500phd: %f, SS_500to1000phd: %f, SS_1000to1500phd: %f\n", SS_100phd,SS_100to500phd,SS_500to1000phd,SS_1000to1500phd);  
    
  fprintf(f, "nMS_et: %f, truthMS_et: %f, MSl/t_et: %f\n", nMS_et, truthMS_et, nMS_et/truthMS_et);
  fprintf(f, "nMS_fid: %f, truthMS_fid: %f, MSl/t_fid: %f\n", nMS_fid, truthMS_fid, nMS_fid/truthMS_fid);
  fprintf(f, "nMS_veto: %f, truthMS_veto: %f, MSl/t_veto: %f\n", nMS_veto, truthMS_veto, nMS_veto/truthMS_veto);
 
  fprintf(f, "MS_100phd: %f, MS_100to500phd: %f, MS_500to1000phd: %f, MS_1000to1500phd: %f\n", MS_100phd,MS_100to500phd,MS_500to1000phd,MS_1000to1500phd);    
    
  fprintf(f, "nKr_et: %f, truthKr_et: %f, Krl/t_et: %f\n", nKr_et, truthKr_et, nKr_et/truthKr_et);
  fprintf(f, "nKr_fid: %f, truthKr_fid: %f, Krl/t_fid: %f\n", nKr_fid, truthKr_fid, nKr_fid/truthKr_fid);
  fprintf(f, "nKr_veto: %f, truthKr_veto: %f, Krl/t_veto: %f\n", nKr_veto, truthKr_veto, nKr_veto/truthKr_veto);

  fprintf(f, "KR_100phd: %f, KR_100to500phd: %f, KR_500to1000phd: %f, KR_1000to1500phd: %f\n", KR_100phd,KR_100to500phd,KR_500to1000phd,KR_1000to1500phd);    
    
  fprintf(f, "nPU_et: %f, truthPU_et: %f, PUl/t_et: %f\n", nPU_et, truthPU_et, nPU_et/truthPU_et);
  fprintf(f, "nPU_fid: %f, truthPU_fid: %f, PUl/t_fid: %f\n", nPU_fid, truthPU_fid, nPU_fid/truthPU_fid);
  fprintf(f, "nPU_veto: %f, truthPU_veto: %f, PUl/t_veto: %f\n", nPU_veto, truthPU_veto, nPU_veto/truthPU_veto);

  fprintf(f, "PU_100phd: %f, PU_100to500phd: %f, PU_500to1000phd: %f, PU_1000to1500phd: %f\n", PU_100phd,PU_100to500phd,PU_500to1000phd,PU_1000to1500phd);    
    
  fprintf(f, "nO_et: %f, truthO_et: %f, Ol/t_et: %f\n", nOther_et, truthO_et, nOther_et/truthO_et);
  fprintf(f, "nO_fid: %f, truthO_fid: %f, Ol/t_fid: %f\n", nOther_fid, truthO_fid, nOther_fid/truthO_fid);
  fprintf(f, "nO_veto: %f, truthO_veto: %f, Ol/t_veto: %f\n", nOther_veto, truthO_veto, nOther_veto/truthO_veto);
    
  fprintf(f, "O_100phd: %f, O_100to500phd: %f, O_500to1000phd: %f, O_1000to1500phd: %f\n", O_100phd,O_100to500phd,O_500to1000phd,O_1000to1500phd);
}
